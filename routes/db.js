//https://github.com/jimmyolo/fastify-mariadb
const bcrypt = require('bcrypt');
var generator = require('generate-password');
var smtpUtils = require('../smtp.js');
var _ = require('lodash');

var routes = async (fastify, options) => {

    fastify.get('/_get/personal_details', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            const cid = req.session._customer_id;

            try {
                const mariadb = fastify.mariadb;
                const connection = await mariadb.getConnection();

                var customer = await mariadb.query(
                    "SELECT * FROM customers WHERE customer_id = ? LIMIT 1", [
                        cid
                    ]).catch(e => {
                        connection.release();
                        console.log(e);
                        throw e;
                    });
                
                customer = customer[0];
                
                var customer_identity = await mariadb.query(
                    "SELECT ci.identity_id, ci.identity_type, ci._number, ci.expiry_issue_date, i._name, i.issued_country_code, i.issued_territory " +
                    "FROM customer_identity ci " +
                    "INNER JOIN identities i ON (ci.identity_type = i.identity_type) " +
                    "WHERE customer_id = ? LIMIT 1", [
                        cid
                    ]).catch(e => {
                        connection.release();
                        console.log(e);
                        throw e;
                    });
                
                customer_identity = customer_identity[0];

                var customer_address = await mariadb.query(
                    "SELECT * FROM personal_addresses a " +
                    "INNER JOIN country_codes c ON (c.country_name = a.country) " +
                    "WHERE customer_id = ? LIMIT 1", [
                        cid
                    ]).catch(e => {
                        connection.release();
                        console.log(e);
                        throw e;
                    });
                
                customer_address = customer_address[0];
                
                connection.release();

                return res.send({
                    statusCode: 200,
                    customer: customer,
                    address: customer_address,
                    identity: customer_identity
                });

            } catch (e) {
                res.status(500);
                return res.send({
                    statusCode: 500,
                    message: e
                });
            }
            

        } else {
            res.status(401);
            return res.send({
                statusCode: 401
            });
        }

    }),

    fastify.post('/_update/personal_details', async (req, res) => {
        
        if (req.isCustomerSessionValid(req)) {
            const cid = req.session._customer_id;
            const mariadb = fastify.mariadb;
            const connection = await mariadb.getConnection();
            var body = req.body;

            /*
            req.body: {
                
                    suffix_title,
                    first_name,
                    middle_name,
                    last_name,
                    DOBmonth,
                    DOBday,
                    DOByear,
                
                    address_line_1,
                    address_line_2,
                    city,
                    _locale,
                    postal_code,
                    country,
                
                    identity_type,
                    identity_number,
                    expiry_issue_date
            }

            */
            
            try {
                var customers_sql = 
                "UPDATE customers SET suffix_title=?, first_name=?, last_name=?, middle_name=?, dob=? WHERE customer_id=? LIMIT 1";

                const customers_result = await mariadb.query(customers_sql, [
                    body.suffix_title.toUpperCase(),
                    body.first_name.toUpperCase(),
                    body.last_name.toUpperCase(),
                    body.middle_name.toUpperCase(),
                    body.DOByear + "-" + `${body.DOBmonth.toString().padStart(2, '0')}` + "-" + `${body.DOBday.toString().padStart(2, '0')}`,
                    cid
                ]);

                var personal_addr_sql =
                "UPDATE personal_addresses SET address_line_1=?, address_line_2=?, city=?, _locale=?, postal_code=?, country=? WHERE customer_id=? LIMIT 1";

                const personal_addr_result = await mariadb.query(personal_addr_sql, [
                    body.address_line_1.toUpperCase(),
                    body.address_line_2.toUpperCase(),
                    body.city.toUpperCase(),
                    body._locale.toUpperCase(),
                    body.postal_code.toUpperCase(),
                    body.country.toUpperCase()
                ]);

                var identity_sql = 
                "UPDATE customer_identity SET identity_type=?, _number=?, expiry_issue_date=? WHERE customer_id=? LIMIT 1";

                const identity_result = await mariadb.query(identity_sql, [
                    body.identity_type,
                    body.identity_number,
                    body.expire_issue_date
                ]);

                connection.release();

                return res.send({
                    statusCode: 200
                });

            } catch (e) {
                console.log(e);
                res.status(500);
                return res.send({
                    statusCode: 500,
                    message: e
                });
            }
        }
        else {
            res.status(401);
            return res.send({
                statusCode: 401
            });
        }
    }),

    fastify.post('/_update/contact_details', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            const cid = req.session._customer_id;
            const mariadb = fastify.mariadb;
            const connection = await mariadb.getConnection();
            var body = req.body;

            /* 
                primary_phone,
                alt_phone,
                email_addr
            */

            try {
                var customers_sql = "UPDATE customers SET primary_phone=?, alt_phone=?, email_addr=? WHERE customer_id=? LIMIT 1";

                const contact_results = await mariadb.query(customers_sql, [
                    body.primary_phone,
                    body.alt_phone,
                    body.email_addr.toUpperCase(),
                    cid
                ]);

                connection.release();

                return res.send({
                    statusCode: 200
                });

            } catch (e) {
                console.log(e);
                res.status(500);
                return res.send({
                    statusCode: 500
                });
            }
        }
        else {
            res.status(401);
            return res.send({
                statusCode: 401
            });
        }
    }),

    fastify.get('/_get/employment_details', async(req, res) => {
        if (req.isCustomerSessionValid(req)) {
            const cid = req.session._customer_id;
            const mariadb = fastify.mariadb;

            try {
                const connection = await mariadb.getConnection();

                emp = await mariadb.query(
                    "SELECT * FROM customer_employment WHERE customer_id = ? LIMIT 1",
                    [cid]).catch(e => {
                        connection.release();
                        throw e;
                    });

                res.status(200);
                return res.send({
                    statusCode: 200,
                    employment: emp[0]
                });
                
            } catch (e) {
                res.status(500);
                res.send({
                    statusCode: 500,
                    message: e
                });
            }

        } else {
            res.status(401);
            return res.send({
                statusCode: 401
            });
        }
    }),

    fastify.post('/_update/employment_details', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            const cid = req.session._customer_id;
            const mariadb = fastify.mariadb;
            const connection = await mariadb.getConnection();
            var body = req.body;

            /* 
                emp_addr_full,
                emp_phone,
                company_name,
                industry,
                _position_name
            */

            try {
                var customers_sql = "UPDATE customer_employment SET emp_addr_full=?, emp_phone=?, company_name=?, industry=?, _position_name=? WHERE customer_id=? LIMIT 1";

                const contact_results = await mariadb.query(customers_sql, [
                    body.emp_addr_full.toUpperCase(),
                    body.emp_phone,
                    body.company_name.toUpperCase(),
                    body.industry.toUpperCase(),
                    body._position_name.toUpperCase(),
                    cid
                ]);

                connection.release();

                return res.send({
                    statusCode: 200
                });

            } catch (e) {
                console.log(e);
                res.status(500);
                return res.send({
                    statusCode: 500
                });
            }
        }
        else {
            res.status(401);
            return res.send({
                statusCode: 401
            });
        }
    }),

    fastify.get('/_get/security_details', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            const cid = req.session._customer_id;
            const mariadb = fastify.mariadb;
            try {
                const connection = await mariadb.getConnection();

                const security = await mariadb.query(
                    "SELECT _2fa_type, username FROM user_auth WHERE customer_id = ? LIMIT 1", [
                        cid
                    ]).catch(e => {
                        connection.release();
                        throw e;
                    });

                connection.release();

                res.status(200);
                return res.send({
                    statusCode: 200,
                    security: security[0]
                });

            } catch (e) {
                res.status(500);
                return res.send({
                    statusCode: 500,
                    message: e
                });
            }
        } else {
            res.status(401);
            return res.send({
                statusCode: 401
            });
        }
    }),

    fastify.post('/_update/security_details', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            const cid = req.session._customer_id;
            const mariadb = fastify.mariadb;
            const body = req.body;

            if (!body._2fa_type || !body.username) {
                res.status(400);
                return res.send({
                    statusCode: 400,
                    message: "Body requires _2fa_type and username"
                });
            }
            else {
                try {
                    const connection = await mariadb.getConnection();
    
                    const security_upd = await mariadb.query(
                        "UPDATE user_auth SET _2fa_type = ?, username = ? WHERE customer_id = ? LIMIT 1", [
                            body._2fa_type,
                            body.username,
                            cid
                        ]).catch(e => {
                            connection.release();
                            throw e;
                        });
    
                    connection.release();
    
                    res.status(200);
                    return res.send({
                        statusCode: 200,
                    });
    
                } catch (e) {
                    res.status(500);
                    return res.send({
                        statusCode: 500,
                        message: e
                    });
                }
            }
        } else {
            res.status(401);
            return res.send({
                statusCode: 401
            });
        }
    }),

    fastify.get('/_get/list_accounts', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            const cid = req.session._customer_id;
            const mariadb = fastify.mariadb;

            try {
                const connection = await mariadb.getConnection();

                var accounts = await mariadb.query('SELECT * FROM tradespace_accounts WHERE customer_id = ? AND NOT _status = ?', [cid, "CLOSED"])
                .catch((e) => {
                    connection.release();
                    throw e;
                });

                connection.release();

                var list = [];

                accounts.forEach(row => {
                    var display = `${row.acct_num} ${row.currency_code}`;

                    list.push({
                        display: display,
                        account: row.acct_num,
                        balance: row.total_bal,
                        available: row.avail_bal,
                        currency_code: row.currency_code
                    });
                });

                return res.send({
                    statusCode: 200,
                    accounts: list
                    // {display: 1234 CAD, value: 1234}
                });

            } catch (e) {
                res.status(500);
                return res.send({
                    statusCode: 500,
                    message: e
                });
            }
        }
        else {
            res.status(401);
            return res.send({
                statusCode: 401
            });
        }
    }),

    fastify.get('/_get/account_transactions', async (req, res) => {

        //needs:?account=1234 

        if (req.isCustomerSessionValid(req)) {

            if (req.query.account) {
                const mariadb = fastify.mariadb;

                var month = req.query.month;
                var year = req.query.year;

                console.log(month);
                console.log(year);

                try {
                    const connection = await mariadb.getConnection();

                    const sqlStatement = 
                        "SELECT transaction_id, acct_num, trans_amount, trans_type, posted_date, _desc FROM tradespace_transactions WHERE acct_num = ? " +
                        "AND posted_date >= ? AND posted_date < ? ORDER BY posted_date LIMIT 200";

                    var postedDate1 = `${year}-${month.toString().padStart(2, "0")}-01`;
                    var postedDate2 = `${year}-${String(parseInt(month.trim())+1).padStart(2, "0")}-01`;

                    console.log(postedDate1);
                    console.log(postedDate2);
                    
                    const transactions = await mariadb.query(sqlStatement, [
                        //ordered by day 1 to day 31
                        req.query.account,
                        postedDate1,
                        postedDate2
                    ]).catch((e) => {
                        connection.release();
                        console.log(e);
                        throw e;
                    });

                    console.log(transactions[0]);

                    connection.release();

                    return res.send({
                        statusCode: 200,
                        trans: transactions
                    });

                } catch (e) {
                    res.status(500);
                    return res.send({
                        statusCode: 500,
                        message: e
                    });
                }   
            }
            else {
                res.status(400);
                return res.send({
                    statusCode: 400,
                    message: "Missing account parameter"
                });
            }
        }
        else {
            res.status(401);
            return res.send({
                statusCode: 401,
                message: "Unauthorized session."
            });
        }
    }),

    fastify.get('/_get/search_transactions', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            if (req.query.term && req.query.account) {
                try {
                    const mariadb = fastify.mariadb;
                    const connection = await mariadb.getConnection();

                    const search_results = await mariadb.query(`SELECT * from tradespace_transactions WHERE acct_num=? AND _desc LIKE '%${req.query.term}%' ORDER BY posted_date DESC LIMIT 50`, [
                        req.query.account,
                        req.query.term
                    ]).catch((e) => {
                        connection.release();

                        console.log(e);
                        
                        res.status(500);
                        return res.send({
                            statusCode: 500
                        });
                    });

                    connection.release();

                    var trans = [];

                    search_results.forEach(row => {
                        var line = {};
                        line.amount = row.trans_amount;
                        line.fx_rate = row.fx_rate;
                        line.trans_type = row.trans_type;
                        line.posted_date = row.posted_date;
                        line.description = row._desc;
                        line.balance = row.balance;
                        trans.push(line);
                    });

                    return res.send({
                        statusCode: 200,
                        search: trans,
                        items: trans.length
                    });

                }
                catch (e) {
                    res.status(500);
                    return res.send({
                        statusCode: 500,
                        message: "Internal error occured."
                    });
                }
            }
            else {
                res.status(400);
                return res.send({
                    statusCode: 400,
                    message: "Period month & year and term parameters required."
                });
            }
        }
        else {
            res.status(401);
            return res.send({
                statusCode: 401,
                message: "Unauthorized session."
            });
        }
    }),

    fastify.get('/_get/list_source_accounts', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            const cid = req.session._customer_id;
            const mariadb = fastify.mariadb;

            try {
                const connection = await mariadb.getConnection();

                var accounts = await mariadb.query(
                    "SELECT s.source_id, s.acct_num, s.currency_code, s.routing_code, b._name AS bank_name, b.country " +
                    "FROM sourcing_accounts s " +
                    "INNER JOIN banks b ON (s.bank_id = b.bank_id) " + 
                    "WHERE s.customer_id = ? LIMIT 100", 
                    [cid]
                ).catch((e) => {
                    connection.release();
                    throw e;
                });

                connection.release();

                var list = [];

                accounts.forEach(row => {

                    list.push({
                        source_id: row.source_id,
                        account: row.acct_num,
                        currency_code: row.currency_code,
                        bank: row.bank_name,
                        country: row.country,
                        routing_code: row.routing_code
                    });
                });

                return res.send({
                    statusCode: 200,
                    accounts: list
                });

            } catch (e) {
                res.status(500);
                return res.send({
                    statusCode: 500,
                    message: e
                });
            }
        }
        else {
            res.status(401);
            return res.send({
                statusCode: 401,
                message: "Unauthorized session."
            });
        }
    }),

    fastify.get('/_get/source_account', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            if (req.query.source_id) {
                const cid = req.session._customer_id;

                try {
                    const mariadb = fastify.mariadb;
                    const connection = await mariadb.getConnection();

                    var source = await mariadb.query(
                        'SELECT s.source_id, s.acct_num, s.currency_code, c.full_name AS currency_name, s.routing_code, s.bank_id, b._name AS bank_name, b.country ' +
                        'FROM sourcing_accounts s ' +
                        'INNER JOIN banks b ON (s.bank_id = b.bank_id) ' + 
                        'INNER JOIN currency c ON (c.currency_code = s.currency_code) ' +
                        'WHERE s.customer_id = ? AND s.source_id = ? LIMIT 1', 
                        [cid, req.query.source_id]
                    ).catch((e) => {
                        connection.release();
                        throw e;
                    });

                    connection.release();

                    return res.send({
                        statusCode: 200,
                        source: {
                            account: source[0].acct_num,
                            currency_code: source[0].currency_code,
                            currency_name: source[0].currency_name,
                            bank: source[0].bank_name,
                            bank_id: source[0].bank_id,
                            country: source[0].country,
                            routing_code: source[0].routing_code,
                            date_added: source[0].date_added
                        }
                    });
                } catch (e) {
                    res.status(500);
                    return res.send({
                        statusCode: 500,
                        message: e
                    });
                }
            }
            else {
                res.status(400);
                return res.send({
                    statusCode: 400,
                    message: "Source account required"
                });
            }
        }
        else {
            res.status(401);
            return res.send({
                statusCode: 401,
                message: "Unauthorized session."
            });
        }
    }),

    fastify.post('/_update/source_account', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            const cid = req.session._customer_id;
            const mariadb = fastify.mariadb;

            var body = req.body;

            try {
                const connection = await mariadb.getConnection();

                var upd = await mariadb.query(
                    'UPDATE sourcing_accounts SET ' +
                    'acct_num=?, ' + 
                    'currency_code=?, ' +
                    'routing_code=?, ' +
                    'bank_id=? ' + 
                    'WHERE customer_id=? AND source_id = ? LIMIT 1',
                    [
                        body.account,
                        body.currency_code,
                        bank.routing_code,
                        body.bank_id,
                        cid,
                        body.source_id
                    ]
                ).catch(e => {
                    connection.release();

                    res.status(500);
                    return res.send({
                        statusCode: 500,
                        message: "Internal error on updating source account"
                    });
                });

                connection.release();

                return res.send({
                    statusCode: 200
                });

            }
            catch (e) {
                res.status(500);
                return res.send({
                    statusCode: 500,
                    message: "Internal error on updating source account"
                });
            }

        } else {
            res.status(401);
            return res.send({
                statusCode: 401,
                message: "Unauthorized session."
            });
        }
    }),

    fastify.post('/_remove/source_account', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            const cid = req.session._customer_id;
            const mariadb = fastify.mariadb;

            var body = req.body;

            try {
                const connection = await mariadb.getConnection();

                var del = await mariadb.query(
                    'DELETE FROM sourcing_accounts WHERE customer_id=? AND source_id=? LIMIT 1',
                    [
                        cid,
                        body.source_id
                    ]
                ).catch(e => {
                    connection.release();

                    res.status(500);
                    return res.send({
                        statusCode: 500,
                        message: "Internal error on removing source account"
                    });
                });

                connection.release();

                return res.send({
                    statusCode: 200,
                    data: del
                });

            } catch (e) {
                res.status(500);
                return res.send({
                    statusCode: 500,
                    message: "Internal error on removing source account"
                });
            }
        } else {
            res.status(401);
            return res.send({
                statusCode: 401,
                message: "Unauthorized session."
            });
        }
    }),

    fastify.get('/_get/recipient/list', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            const cid = req.session._customer_id;
            const mariadb = fastify.mariadb;

            try {
                const connection = await mariadb.getConnection();

                const recips = await mariadb.query(
                    "SELECT r_id, r.dest_acct, a.currency_code, c.suffix_title, c.first_name, c.middle_name, c.last_name, r.email_addr " +
                    "FROM tradespace_recipients r " +
                    "INNER JOIN customers c ON (r.recipient_id = c.customer_id) " +
                    "INNER JOIN tradespace_accounts a ON (a.acct_num = r.dest_acct) " +
                    "WHERE owner_id = ? LIMIT 200", [
                        cid
                    ]).catch(e => {
                        connection.release();
                        throw e;
                    });

                connection.release();

                res.send({
                    statusCode: 200,
                    recips: recips
                });

            } catch (e) {
                res.status(500);
                return res.send({
                    statusCode: 500,
                    message: e
                });
            }

        } else {
            res.status(401);
            return res.send({
                statusCode: 401
            });
        }
    }),

    fastify.get('/_get/recipient', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            const cid = req.session._customer_id;
            const mariadb = fastify.mariadb;

            if (req.query.r_id) {
                try {
                    const connection = await mariadb.getConnection();
    
                    var row = await mariadb.query(
                        "SELECT c.suffix_title, c.first_name, c.middle_name, c.last_name, " +
                        "r.dest_acct, cu.currency_code, cu.full_name AS currency_name, " +
                        "r.email_addr, i.issued_country_code, i.identity_type, r.identity_num, r.identity_issue_expiry_date, cc.country_name, i._name " + 
                        "FROM tradespace_recipients r " +
                        "INNER JOIN customers c ON (r.recipient_id = c.customer_id) " +
                        "INNER JOIN tradespace_accounts a ON (r.dest_acct = a.acct_num) " +
                        "INNER JOIN identities i ON (r.identity_index = i.identity_type) " +
                        "INNER JOIN country_codes cc On (cc.country_code = i.issued_country_code) " +
                        "INNER JOIN currency cu ON (cu.currency_code = a.currency_code)" +
                        "WHERE r.r_id = ? LIMIT 1",
                        [
                            req.query.r_id
                        ]
                    ).catch(e => {
                        connection.release();
                        throw e;
                    });

                    console.log(row[0]);
    
                    connection.release();
    
                    return res.send({
                        statusCode: 200,
                        recip: row[0]
                    });
    
                } catch (e) {
                    res.status(500);
                    return res.send({
                        statusCode: 500,
                        message: e
                    });
                }
            } else {
                res.status(400);
                return res.send({
                    statusCode: 400,
                    message: "r_id parameter required"
                });
            }

        } else {
            res.status(401);
            return res.send({
                statusCode: 401,
                message: "Unauthorized session."
            });
        }
    }),

    fastify.post('/_update/recipient', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            const cid = req.session._customer_id;
            const mariadb = fastify.mariadb;

            var body = req.body;

            try {
                const connection = await mariadb.getConnection();
                
                var upd = await mariadb.query(
                    'UPDATE tradespace_recipients SET ' +
                    'dest_acct = ?, ' + 
                    'email_addr = ?, ' +
                    'identity_index = ?, ' +
                    'identity_num = ?, ' + 
                    'identity_issue_expiry_date = ? ' +
                    'WHERE r_id = ? LIMIT 1',
                    [
                        body.dest_acct,
                        body.email.toUpperCase(),
                        body.identity_index,
                        body.identity_num,
                        body.identity_date,
                        body.index
                    ]).catch(e => {
                        connection.release();
                        throw e;
                    });

                connection.release();

                return res.send({
                    statusCode: 200,
                    index: body.index
                });

            } catch (e) {
                connection.release();
    
                res.status(500);
                return res.send({
                    statusCode: 500,
                    message: "Internal error on updating recipient"
                });
            }


        } else {
            res.status(401);
            return res.send({
                statusCode: 401,
                message: "Unauthorized session."
            });
        }
    }),

    fastify.post('/_create/recipient', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            const cid = req.session._customer_id;
            const mariadb = fastify.mariadb;

            console.log(req.body);

            try {
                const connection = await mariadb.getConnection();

                var recipIdFind = await mariadb.query(
                    "SELECT customer_id FROM tradespace_accounts WHERE acct_num = ? LIMIT 1", [
                    req.body.dest_acct
                ]).catch(e => {
                    connection.release();
                    throw e;
                });

                recipIdFind = recipIdFind[0].customer_id;

                var create_sql = "INSERT INTO tradespace_recipients SET " +
                    "owner_id = ?, recipient_id = ?, dest_acct = ?, email_addr = ?";

                var create = await mariadb.query(
                    create_sql,
                    [
                        cid,
                        recipIdFind,
                        req.body.dest_acct,
                        req.body.email.toUpperCase()
                    ]
                ).catch(e => {
                    connection.release();
                    throw e;
                });

                connection.release();

                if (create.affectedRows != 0) {
                    res.status(201);
                    return res.send({
                        statusCode: 201,
                        message: "Recipient created."
                    });
                }
                else {
                    throw "Error creating recipient record"
                }

            } catch (e) {
                res.status(500);
                return res.send({
                    statusCode: 500,
                    message: e
                });
            }
        } else {
            res.status(401);
            return res.send({
                statusCode: 401,
                message: "Unauthorized session."
            });
        }
    }),

    fastify.post('/_create/transfer', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            const cid = req.session._customer_id;
            const mariadb = fastify.mariadb;

            //create transfer record
            //create DR transaction record on origin acct

            try {
                const connection = await mariadb.getConnection();

                var transfer_sql = "INSERT INTO tradespace_transfers " +
                    "(transfer_id, origin_acct, dest_acct, recip_index, origin_currency, dest_currency, fx_rate, amount, secret_hash, memo) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

                if (!req.body.origin_acct) {
                    throw "Origin account not specified";
                }

                if (!req.body.dest_acct) {
                    throw "Destination account not specified";
                }

                if (!req.body.recip_index) {
                    throw "Recipient ID index not specified";
                }

                if (!req.body.origin_currency) {
                    throw "Origin account's currency not specified";
                }

                if (!req.body.dest_currency) {
                    throw "Destination account's currency not specified";
                }

                if (!req.body.fx_rate) {
                    throw "FX rate is not a number";
                }

                if (!req.body.secret) {
                    throw "Transfer passphrase not specified";
                }

                if (!req.body.amount) {
                    throw "Transfer dollar amount not specified";
                }

                if (!req.body.recipient_name) {
                    throw "Recipient's name not specified";
                }

                if (!req.body.recipient_email) {
                    throw "Recipient's email address not specified";
                }

                var transfer_uuid = _.random(1000000, 9999999);

                var secret_hash = await bcrypt.hash(req.body.secret, 10);
                
                var transfer_query = await mariadb.query(transfer_sql, [
                    transfer_uuid,
                    req.body.origin_acct,
                    req.body.dest_acct,
                    req.body.recip_index,
                    req.body.origin_currency,
                    req.body.dest_currency,
                    parseFloat(req.body.fx_rate).toFixed(4),
                    parseFloat(req.body.amount).toFixed(2),
                    secret_hash,
                    req.body.memo.substring(0, 255)
                ]).catch(e => {
                    console.log(e);
                    throw e;
                });

                var _desc = `${req.body.recipient_name} ${req.body.recipient_email} REF ${transfer_uuid}`;

                var transaction_sql = "INSERT INTO tradespace_transactions " +
                    "(transfer_id, acct_num, trans_amount, trans_type, _desc) " +
                    "VALUES (?, ?, ?, ?, ?)";

                var transaction_query = await mariadb.query(transaction_sql, [
                    transfer_uuid,
                    req.body.origin_acct,
                    parseFloat(req.body.amount).toFixed(2),
                    "DR",
                    _desc.substring(0,255)
                ]).catch(e => {
                    console.log(e);
                    throw e;
                });

                var get_OriginAcct = await mariadb.query(
                    "SELECT total_bal, avail_bal FROM tradespace_accounts " +
                    "WHERE acct_num = ? LIMIT 1", [
                        req.body.origin_acct
                    ]).catch(e => {
                        console.log(e);
                        throw e;
                    });
                
                get_OriginAcct = get_OriginAcct[0];

                var newTotalBal = (parseFloat(get_OriginAcct.total_bal) - parseFloat(req.body.origin_amount)).toFixed(2);
                var newAvailBal = (parseFloat(get_OriginAcct.avail_bal) - parseFloat(req.body.origin_amount)).toFixed(2);

                const upd_OriginAcct = await mariadb.query(
                    "UPDATE tradespace_accounts SET " +
                    "total_bal = ?, avail_bal = ? " +
                    "WHERE acct_num = ? LIMIT 1", [
                        newTotalBal,
                        newAvailBal,
                        req.body.origin_acct
                    ]).catch(e => {
                        console.log(e);
                        throw e;
                    });

                connection.release();

                if (transaction_query.affectedRows > 0) {

                    //send transfer email

                    await smtpUtils.sendTransferNotification({
                        recip_name: req.body.recipient_name,
                        to_addr: req.body.recipient_email,
                        sender_addr: req.body.sender_email.toLowerCase(),
                        sender_name: req.body.sender_name,
                        amount: req.body.amount,
                        dest_currency: req.body.dest_currency.toUpperCase(),
                        memo: req.body.memo.substring(0, 255),
                        transfer_id: transfer_uuid
                    }).catch(e => {
                        console.log(e);
                        throw e;
                    });

                    res.status(201);
                    return res.send({
                        statusCode: 201,
                        transfer_id: transfer_uuid
                    });
                }
                else {
                    res.status(500);
                    return res.send({
                        statusCode: 500,
                        message: "Internal Error creating transfer"
                    });
                }

            } catch (e) {
                console.log(e);

                connection.release();

                res.status(500);
                return res.send({
                    statusCode: 400,
                    message: e
                });
            }
        } else {
            res.status(401);
            return res.send({
                statusCode: 401,
                message: "Unauthorized session."
            });
        }
    }),

    fastify.post('/_create/onboarding/customer', async (req, res) => {

        var body = req.body;

        if (!body) {
            res.status(400);
            return res.send({
                statusCode: 400,
                message: "Invalid body"
            });
        }

        Object.keys(body).forEach(k => {
              
            if (typeof body[k] == 'string' && !["username", "passwd", "customer_email_addr"].includes(k)) {
                body[k] = body[k].toString().trim().toUpperCase();
            }

            if ([null, "null", "NULL", "", " "].includes(body[k])) {
                body[k] = null;
            }

        });

        try {
            const mariadb = fastify.mariadb;
            const connection = await mariadb.getConnection();

            var cid = _.random(1000000, 9999999);
            
            /* var cid = await mariadb.query("SELECT UUID_SHORT() AS short_uuid")
                .catch(e => {
                    connection.release();

                    console.log(e);
                    throw new Error(e);
                });

            cid = cid[0].short_uuid; */

            const countries = await mariadb.query("SELECT * FROM country_codes LIMIT 200")
                .catch(e => {
                    connection.release();

                    console.log(e);
                    throw new Error(e);
                });
            
            body.personal_address_country = countries.find(row => {
                if (row.country_code.includes(body.personal_address_country.toUpperCase())) {
                    return true;
                }
                else {
                    return false;
                }
            }).country_name;

            var insertCustomer = await mariadb.query(
                "INSERT INTO customers " +
                "(customer_id, first_name, middle_name, last_name, suffix_title, dob, primary_phone, email_addr) " + 
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)", [
                    cid,
                    body.customer_first,
                    body.customer_middle,
                    body.customer_last,
                    body.customer_suffix,
                    body.customer_dob,
                    body.customer_primary_phone,
                    body.customer_email_addr
                ]).catch(e => {
                    connection.release();

                    console.log(e);
                    throw new Error(e);
                });
            
            
            var insertPersonalAddress = await mariadb.query(
                "INSERT INTO personal_addresses " +
                "(customer_id, address_line_1, address_line_2, city, _locale, postal_code, country) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?)", [
                    cid,
                    body.personal_address_line1,
                    body.personal_address_line2,
                    body.personal_address_city,
                    body.personal_address_locale,
                    body.personal_address_postal,
                    body.personal_address_country,
                ]).catch(e => {
                    connection.release();

                    console.log(e);
                    throw new Error(e);
                });

            var insertEmployment = await mariadb.query(
                "INSERT INTO customer_employment " +
                "(customer_id, emp_addr_full, emp_phone, company_name, industry, _position_name) " +
                "VALUES (?, ?, ?, ?, ?, ?)", [
                    cid,
                    body.emp_address,
                    body.emp_phone,
                    body.emp_company,
                    body.emp_industry,
                    body.emp_position
                ]).catch(e => {
                    connection.release();

                    console.log(e);
                    throw new Error(e);
                });
            
            var insertIdentity = await mariadb.query(
                "INSERT INTO customer_identity " +
                "(customer_id, _number, expiry_issue_date, identity_type) " +
                "VALUES (?, ?, ?, ?)", [
                    cid,
                    body.identity_number,
                    body.identity_date,
                    body.identity_type
                ]).catch(e => {
                    connection.release();

                    console.log(e);
                    throw new Error(e);
                });
            
            const newAcctNum = generator.generate({
                length: 12,
                numbers: true,
                symbols: false,
                lowercase: false,
                uppercase: false,
                exclude: "abcdefghijklmnopqrstuvwxyz"
            });

            var insertAccount = await mariadb.query(
                "INSERT into tradespace_accounts " +
                "(acct_num, customer_id, currency_code) " +
                "VALUES (?, ?, ?)", [
                    31200 + newAcctNum,
                    cid,
                    body.account_currency
                ]).catch(e => {
                    connection.release();

                    console.log(e);
                    throw new Error(e);
                });
            
            const hash = await bcrypt.hash(body.passwd, 10);
            
            var insertUserAuth = await mariadb.query(
                "INSERT INTO user_auth " +
                "(customer_id, username, passwd_hash) " +
                "VALUES (?, ?, ?)", [
                    cid,
                    body.username,
                    hash
                ]).catch(e => {
                    connection.release();

                    console.log(e);
                    throw new Error(e);
                });

            connection.release();
            
            res.status(201);
            return res.send({
                statusCode: 201,
                cid: cid
            });

        } catch (e) {

            res.status(500);
            res.send({
                statusCode: 500,
                message: e.message
            });
        }
    }),

    fastify.get("/_get/transfer/info", async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            const cid = req.session._customer_id;
            const mariadb = fastify.mariadb;

            try {
                if (!req.query.transfer_id) {
                    throw "transfer_id param required"
                }

                var connection = await mariadb.getConnection();

                const transferSql = await mariadb.query(
                    "SELECT a.customer_id, t.dest_acct, t.amount, t.dest_currency, t.memo, t._status " +
                    "FROM tradespace_transfers t " +
                    "INNER JOIN tradespace_accounts a ON (t.dest_acct = a.acct_num) " +
                    "WHERE t.transfer_id = ? LIMIT 1", [
                        req.query.transfer_id
                    ]).catch(e => {
                        console.log(e);
                        throw e;
                    });
                
                if (transferSql[0].customer_id != cid) {
                    res.status(401);
                    return res.send({
                        statusCode: 401,
                        message: "customer logged in is not the recipient."
                    });
                } else if (transferSql[0]._status != "PENDING") {
                    res.status(404);
                    return res.send({
                        statusCode: 404,
                        message: "The transfer is completed, cancelled or does not exist"
                    });
                } else {

                    const senderSql = await mariadb.query(
                        "SELECT c.suffix_title, c.first_name, c.middle_name, c.last_name " + 
                        "FROM tradespace_transfers t " +
                        "INNER JOIN tradespace_accounts a ON (t.origin_acct = a.acct_num) " +
                        "INNER JOIN customers c ON (c.customer_id = a.customer_id) " + 
                        "WHERE t.transfer_id = ? LIMIT 1", [
                            req.query.transfer_id
                        ]).catch(e => {
                            console.log(e);
                            throw e;
                        });
                    
                    var senderName = `${senderSql[0].suffix_title} ${senderSql[0].first_name}`;
                    if (senderSql[0].middle_name != null) {
                        senderName += ` ${senderSql[0].middle_name}`;
                    }
                    senderName += ` ${senderSql[0].last_name}`;
    
                    connection.release();
    
                    return res.send({
                        statusCode: 200,
                        sender_name: senderName,
                        transfer: transferSql[0]
                    });
                }
                

            } catch (e) {
                console.log(e);
                res.status(500);
                return res.send({
                    statusCode: 500,
                    message: e
                });
            }

        } else {
            res.status(401);
            return res.send({
                statusCode: 401
            });
        }
    }),

    fastify.post('/_update/transfer', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            const cid = req.session._customer_id;
            const mariadb = fastify.mariadb;

            try {
                var connection = await mariadb.getConnection();

                var checkSql = await mariadb.query(
                    "SELECT a.customer_id, t.secret_hash, t.amount, t.dest_acct, t._status, t.fx_rate, t.origin_currency " +
                    "FROM tradespace_transfers t " +
                    "INNER JOIN tradespace_accounts a ON (t.dest_acct = a.acct_num) " +
                    "WHERE transfer_id = ? LIMIT 1", [
                        req.body.transfer_id
                    ]).catch(e => {
                        console.log(e);
                        throw e;
                    });

                if (checkSql[0].customer_id != cid) {
                    connection.release();

                    res.status(401);
                    return res.send({
                        statusCode: 401,
                        message: "You are not recipient of this transfer."
                    });
                } else {

                    if (checkSql[0]._status != "PENDING") {
                        connection.release();
                        res.status(404);
                        return res.send({
                            statusCode: 404,
                            message: "Transfer already completed, cancelled, or expired"
                        });
                    } else {
                        var secretMatch = await bcrypt.compare(req.body.secret, checkSql[0].secret_hash);

                        if (secretMatch) {

                            var updTransfer = await mariadb.query(
                                "UPDATE tradespace_transfers SET _status = ? WHERE transfer_id = ? LIMIT 1", [
                                    "COMPLETE",
                                    req.body.transfer_id
                                ]).catch(e => {
                                    console.log(e);
                                    throw e;
                                });

                            //tradespace CR
                            var originAmount = parseFloat(parseFloat(checkSql[0].amount) * parseFloat(checkSql[0].fx_rate)); 

                            var crRecord = await mariadb.query(
                                "INSERT INTO tradespace_transactions " +
                                "SET transfer_id = ?, acct_num = ?, trans_amount = ?, trans_type = ?, _desc = ?", [
                                    req.body.transfer_id,
                                    checkSql[0].dest_acct,
                                    originAmount.toFixed(2),
                                    "CR",
                                    `Transfer Credit (${checkSql[0].amount} ${checkSql[0].origin_currency}) REF ${req.body.transfer_id}`
                                ]).catch(e => {
                                    console.log(e);
                                    throw e;
                                });

                            connection.release();

                            res.status(200);
                            return res.send({
                                statusCode: 200,
                                transfer: true
                            });

                        } else {
                            connection.release();

                            res.status(401);
                            return res.send({
                                statusCode: 401,
                                secret: false,
                                message: "Secret Passphrase does not match."
                            });
                        }
                    }
                }

            } catch (e) {
                console.log(e);
                res.status(500);
                return res.send({
                    statusCode: 500,
                    message: e
                });
            }

        } else {
            res.status(401);
            return res.send({
                statusCode: 401
            });
        }
    })

}

module.exports = routes;