const bcrypt = require('bcrypt');
var generator = require('generate-password');

var routes = async (fastify, options) => {
    fastify.post('/login', async(req, res) => {
        const {username, passwd} = req.body;

        if (username.length == 0 || passwd.length == 0) {
            res.status(400);
            return res.send({
                statusCode: 400,
                message: "Missing body parameters"
            });
        }

        //use bcrypt to hash passwords

        //check against database
        const mariadb = fastify.mariadb;
        const connection = await mariadb.getConnection();
        const db_result = await mariadb.query(
            'SELECT * FROM user_auth WHERE username = ? LIMIT 1', [username]
        ).catch(e => console.log(e));
        connection.release();

        if (db_result.length == 1) {

            if (db_result[0].username) {
                //use bcrypt to compare password
                if (await bcryptComparePasswd(passwd, db_result[0].passwd_hash)) {
                    
                    req.session.set('_authenticated', true);
                    req.session.set('_customer_id', db_result[0].customer_id);
                    
                    return res.send({
                        statusCode: 200,
                        login: true
                    });
                }
                else {
                    res.status(401);
                    return res.send({
                        statusCode: 401,
                        message: "Invalid credentials."
                    });
                }
            }
            else {
                res.status(401);
                return res.send({
                    statusCode: 401,
                    message: "Unknown username."
                });
            }
        }
        else {
            res.status(500);
            return res.send({
                statusCode: 401,
                message: 'No customer found.'
            });
        }
    }),

    fastify.get('/logout', async(req, res) => {

        if (req.session._authenticated) {
            var err = await req.session.destroy();
            if (err) {
                res.status(500);
                return res.send({
                    statusCode: 500,
                    msg: 'Internal Error. Could not log out.'
                });
            }
            else {
                return res.redirect('/login');
            }
        }
        else {
            return res.redirect('/login');
        }  
    }),

    fastify.post('/recovery', async(req, res) => {
        const { 
            username, 
            lastTransferDate, 
            recipEmail, 
            customerMonth, 
            customerYear
        } = req.body;

        try {
            const mariadb = fastify.mariadb;
            const connection = await mariadb.getConnection();

            if (!username || username.length != 0) {
                throw new Error("username not specified");
            }

            if (!lastTransferDate || lastTransferDate.length != 0) {
                throw new Error("Transfer date not specified");
            }

            if (!recipEmail || recipEmail.length != 0) {
                throw new Error("Recipient Contact email not specified");
            }

            if (!customerMonth || customerMonth.length != 0) {
                throw new Error("Customer Since month not specified");
            }

            if (!customerYear || customerYear.length != 0) {
                throw new Error("Customer Since year not specified");
            }

            let score = 0;

            const _user = await mariadb.query("SELECT username, customer_id WHERE username = ? LIMIT 1", [username.toLowerCase()])
            .catch(e => {
                connection.release();

                res.status(500);
                return res.send({
                    statusCode: 500,
                    message: 'Internal Error finding username'
                });
            });

            if (_user[0].username.toLowerCase() != username.toLowerCase()) {
                connection.release();
                throw new Error("Username not found. Unable to recover customer.");
            }
            else {
                const lastTransfers = await mariadb.query(
                    "SELECT DISTINCT t.`posted_date` FROM tradespace_transactions t " +
                    "INNER JOIN tradespace_accounts a ON (t.`acct_num` = a.`acct_num`) " +
                    "WHERE t.`trans_type` = ? AND t.`transfer_id` IS NOT NULL AND a.`customer_id` = ? " +
                    "AND ORDER BY t.`posted_date` ASC " +
                    "LIMIT 5",
                    [
                        "DR",
                        _user[0].customer_id
                    ]).catch(e => {
                        res.status(500);
                        return res.send({
                            statusCode: 500,
                            message: 'Internal Error finding transfer dates from accounts'
                        });
                    });
                
                const recipEmails = await mariadb.query(
                    "SELECT DISTINCT email_addr FROM tradespace_recipients WHERE owner_id=? AND email_addr = ? LIMIT 1", [
                    _user[0].customer_id,
                    recipEmail.toUpperCase()
                ]).catch(e => {
                    connection.release();

                    res.status(500);
                    return res.send({
                        statusCode: 500,
                        message: 'Internal Error finding recipients by email'
                    });
                });
                
                const customerSince = await mariadb.query(
                    "SELECT DISTINCT created_at FROM customers WHERE customer_id = ? LIMIT 1", [_user[0].customer_id]
                ).catch(e => {
                    connection.release();

                    res.status(500);
                    return res.send({
                        statusCode: 500,
                        message: 'Internal Error finding customer since by customer id'
                    });
                });

                lastTransfers.forEach(row => {
                    if (row.posted_date == lastTransferDate) {
                        score ++;
                    }
                });

                recipEmails.forEach(row => {
                    if (row.email_addr == recipEmail) {
                        score ++;
                    }
                });

                customerSince.forEach(row => {
                    if (row.created_at.includes(`${customerYear}-${customerMonth}-`)) {
                        score ++;
                    }
                });

                if (score >= 2) {

                    //create temporary passwd
                    var tempPassPlain = generator.generate({
                        length: 12,
                        numbers: true,
                        symbols: false,
                        excludeSimilarCharacters: true,
                        strict: true
                    });

                    var recoveryKey = generator.generate({
                        length: 64,
                        numbers: true,
                        symbols: false,
                        excludeSimilarCharacters: true,
                        strict: true
                    });

                    var hash = await bcrypt.hash(tempPassPlain, 10);

                    var temp_sql = await mariadb.query(
                        "UPDATE user_auth SET passwd_hash = ?, recovery_key = ? WHERE username = ? LIMIT 1", [
                        hash,
                        recoveryKey,
                        _user[0].username
                    ]).catch(e => {
                        connection.release();

                        res.status(500);
                        return res.send({
                            statusCode: 500,
                            message: 'Internal Error creating temp password and recovery key'
                        });
                    });

                    connection.release();

                    return res.send({
                        statusCode: 200,
                        recovery_test: "passed",
                        recovery_key: recoveryKey,
                        temp_passwd: tempPassPlain
                    });
                }
                else {
                    connection.release();

                    //call customer care

                    res.status(401);
                    return res.send({
                        statusCode: 401,
                        recovery_test: "failed"
                    });
                }
            }
    
        } catch (e) {
            res.status(400);
            return res.send({
                statusCode: 400,
                message: e.message
            });
        }
    }),

    fastify.post('/recovery_username', async(req, res) => {
        const body = req.body;

        if (body.email &&
            body.dob &&
            body.first_name &&
            body.last_name &&
            body.account) {

            try {
                const mariadb = fastify.mariadb;
                const connection = await mariadb.getConnection();

                const _username = await mariadb.query(
                    "SELECT u.`username` " + 
                    "FROM user_auth u " +
                    "INNER JOIN tradespace_accounts a ON (a.customer_id = u.customer_id) " +
                    "INNER JOIN customers c ON (c.customer_id = a.customer_id) " +
                    "WHERE c.email_addr = ? " +
                    "c.dob = ? AND " +
                    "c.first_name = ? AND " +
                    "c.last_name = ? AND " +
                    "a.acct_num = ? LIMIT 1", [
                        body.email.toUpperCase(),
                        body.dob,
                        body.first_name.toUpperCase(),
                        body.last_name.toUpperCase(),
                        body.account
                ]).catch(e => {
                    connection.release();

                    res.status(500);
                    return res.send({
                        statusCode: 500,
                        message: 'Internal Error processing username recovery.'
                    });
                });

                if (_username[0].username) {
                    return res.send({
                        statusCode: 200,
                        username: _username[0].username
                    });
                }
                else {
                    res.status(404);
                    return res.send({
                        statusCode: 404,
                        message: "No username found with given body request."
                    });
                }


            } catch (e) {
                res.status(500);
                return res.send({
                    statusCode: 500,
                    message: "Internal Error processing username recovery."
                });
            }
        } else {
            res.status(400);
            return res.send({
                statusCode: 400,
                message: "Missing body. Email, dob, first_name, last_name, account required."
            });
        }
    }),

    fastify.post('/recovery/reset', async(req, res) => {
        const body = req.body;
        if (body.recovery_key.length == 64 && body.temp_passwd && body.new_passwd) {
            try {
                const mariadb = fastify.mariadb;
                const connection = await mariadb.getConnection();

                const customer = await mariadb.query("SELECT * FROM user_auth WHERE recovery_key = ? LIMIT 1", [
                    body.recovery_key
                ]).catch(e => {
                    connection.release();
    
                    res.status(500);
                    return res.send({
                        statusCode: 500,
                        message: "Error getting customer by recovery key."
                    });
                });

                //if temp passwd is same
                if (await bcryptComparePasswd(body.temp_passwd, customer[0].passwd_hash)) {
                    var hash = await bcrypt.hash(body.new_passwd, 10);

                    //update to new passwd
                    const newPass = await mariadb.query('UPDATE user_auth SET passwd_hash = ?, recovery_key = ? WHERE recovery_key = ? LIMIT 1', [
                        hash,
                        null,
                        body.recovery_key
                    ]).catch(e => {
                        connection.release();
    
                        res.status(500);
                        return res.send({
                            statusCode: 500,
                            message: "Error setting new password from recovery."
                        });
                    });

                    connection.release();

                    return res.send({
                        statusCode: 200,
                        finalize: true
                    });

                }
                else {
                    connection.release();

                    res.status(401);
                    return res.send({
                        statusCode: 401,
                        message: "Temporary password does not match our records."
                    });
                }

            } catch (e) {
                res.status(500);
                return res.send({
                    statusCode: 500,
                    message: "Internal Error processing temp password recovery set."
                });
            }

        }
        else {
            res.status(400);
            return res.send({
                statusCode: 400,
                message: "Missing body, recovery_key, temp_passwd, new_passwd"
            });
        }
    })
}

var bcryptComparePasswd = async (plain, hash) => {
    const match = await bcrypt.compare(plain, hash);
    return match;
}

module.exports = routes;