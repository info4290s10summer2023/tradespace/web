const path = require('path');
const fs = require('fs');
const cheerio = require('cheerio');
const _forex = require('../forex.js');

var routes = async (fastify, options) => {
    fastify.get('/', async (req, res) => {
        return res.sendPage(path.join(__dirname, '../', 'pages', 'index.html'));
    }),

    fastify.get('/index.html', async (req, res) => {
        res.status(301);
        return res.redirect("/");
    }),

    fastify.get('/login', async (req, res) => {
        return res.sendPage(path.join(__dirname, '../', 'pages', 'login.html'));
    }),

    fastify.get('/login.html', async (req, res) => {
        res.status(301);
        return res.redirect("/login");
    }),

    fastify.get('/accounts', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {

            try  {
                return res.sendPage(path.join(__dirname, '../', 'pages', 'accounts.html'));
            }
            catch (e) {
                fastify.log.error(e);
                
                res.status(500);
                return res.send({
                    statusCode: 500,
                    message: e
                });
            }
        }
        else {
            return res.redirect('/login');
        }
    }),

    fastify.get('/accounts.html', async (req, res) => {
        res.status(301);
        return res.redirect("/accounts");
    }),

    fastify.get('/profile', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            try {
                return res.sendPage(path.join(__dirname, '../', 'pages', 'profile.html'));
            }
            catch (e) {
                fastify.log.error(e);
                
                res.status(500);
                return res.send({
                    statusCode: 500,
                    message: e
                });
            }
        }
        else {
            return res.redirect('/login');
        }
    }),

    fastify.get('/profile.html', async (req, res) => {
        res.status(301);
        return res.redirect("/profile");
    }),

    fastify.get('/_db/list/identities', async (req, res) => {
        //public facing no api key needed
        //get list of all acceptable identification types
        try {
            const mariadb = fastify.mariadb;
            const connection = await mariadb.getConnection();
            const identity_list = await mariadb.query('SELECT identity_type AS _value, _name FROM identities');
            connection.release();
    
            // [{_value: 123, _name: 'foobar'}, {_value: 456, _name: 'fubar'}]

            return res.allowAllDomainsCORS().send({
                statusCode: 200,
                identities: identity_list
            });

        } catch (e) {
            fastify.error.log(e);
            res.status(500).send({
                statusCode: 500
            });
        }

    }),

    fastify.get('/_db/list/identity_types', async (req, res) => {
        //public facing no api key needed
        //get list of all acceptable identification types by country

        if (req.query.country_code) {
            try {
                const mariadb = fastify.mariadb;
                const connection = await mariadb.getConnection();
                const identity_list = await mariadb.query(
                    'SELECT identity_type AS _value, _name, issued_territory AS _territory FROM identities ' +
                    'WHERE issued_country_code = ? LIMIT 100', [
                        req.query.country_code.toUpperCase()
                    ]).catch(e => {
                        connection.release();
                        throw e;
                    });

                connection.release();
        
                // [{_value: 123, _name: 'foobar'}, {_value: 456, _name: 'fubar'}]
    
                return res.allowAllDomainsCORS().send({
                    statusCode: 200,
                    identities: identity_list
                });
    
            } catch (e) {
                res.status(500).send({
                    statusCode: 500,
                    message: e
                });
            }
        }
        else {
            res.status(400);
            return res.send({
                statusCode: 400,
                message: "Missing country_code parameter"
            });
        }

    }),

    fastify.get('/transactions', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            try {
                return res.sendPage(path.join(__dirname, '../', 'pages', 'transactions.html'));
            }
            catch (e) {
                fastify.log.error(e);
                
                res.status(500);
                return res.send({
                    statusCode: 500,
                    message: e
                });
            }
        }
        else {
            return res.redirect('/login');
        }
    }),

    fastify.get('/transactions.html', async (req, res) => {
        res.status(301);
        return res.redirect("/transactions");
    }),

    fastify.get('/transfer', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            try {
                const file = fs.readFileSync(path.join(__dirname, '../', 'pages', 'transfer.html'), 'utf-8');
                const $ = cheerio.load(file);

                const cid = req.session._customer_id;
                const mariadb = fastify.mariadb;
                const connection = await mariadb.getConnection();

                const customer_result = await mariadb.query(
                    'SELECT * FROM customers WHERE customer_id = ? LIMIT 1', 
                    [cid]
                ).catch(e => {
                    console.log(e);
                    connection.release();
                });

                connection.release();

                var displayName = customer_result[0].suffix_title.toUpperCase() + " ";
                displayName += customer_result[0].first_name.toUpperCase() + " ";
                if (customer_result[0].middle_name != null) {
                    displayName += customer_result[0].middle_name.toUpperCase() + " ";
                }
                displayName += customer_result[0].last_name.toUpperCase() + " ";

                $('#displayName').val(displayName);

                $('#displayEmail').val(customer_result[0].email_addr.toLowerCase());

                var rendered = $.html();

                return res.type('text/html').send(rendered);

            } catch (e) {
                fastify.log.error(e);
                
                res.status(500);
                return res.send({
                    statusCode: 500,
                    message: e
                });
            }
        }
        else {
            return res.redirect('/login');
        }
    }),

    fastify.get('/transfer.html', async (req, res) => {
        res.status(301);
        return res.redirect("/transfer");
    }),

    fastify.get('/_db/list/bank_countries', async (req, res) => {

        try {
            const mariadb = fastify.mariadb;
            const connection = await mariadb.getConnection();

            var countries = await mariadb.query(
                'SELECT DISTINCT country from banks LIMIT 200',
                []
            ).catch(e => {
                connection.release();
                throw e;
            });

            connection.release();

            var c = [];

            countries.forEach(row => {
                c.push(row.country);
            });

            return res.send({
                statusCode: 200,
                countries: c
            });


        } catch (e) {
            res.status(500);
            return res.send({
                statusCode: 500,
                message: e
            });
        }
    }),

    fastify.get('/_db/list/bank', async (req, res) => {

        if (req.query.country) {
            try {
                const mariadb = fastify.mariadb;
                const connection = await mariadb.getConnection();
    
                var banks = await mariadb.query(
                    'SELECT bank_id, _name FROM banks WHERE country = ?',
                    [req.query.country]
                ).catch(e => {
                    connection.release();
    
                    res.status(500);
                    return res.send({
                        statusCode: 500,
                        message: "Error getting banks by country"
                    });
                });
    
                connection.release();
    
                var b = [];
    
                banks.forEach(row => {
                    b.push({
                        name: row._name,
                        id: row.bank_id
                    });
                });
    
                return res.send({
                    statusCode: 200,
                    banks: b
                });
    
    
            } catch (e) {
                res.status(500);
                return res.send({
                    statusCode: 500,
                    message: "Internal Error"
                });
            }
        }
        else {
            res.status(400);
            return res.send({
                statusCode: 400,
                message: "Missing country parameter"
            });
        }
    }),

    fastify.get('/_db/list/currencies', async (req, res) => {
        try {
            const mariadb = fastify.mariadb;
            const connection = await mariadb.getConnection();

            var currencies = await mariadb.query(
                'SELECT * FROM currency LIMIT 200',
                []
            ).catch(e => {
                connection.release();
                throw e;
            });

            connection.release();

            return res.send({
                statusCode: 200,
                currency: currencies
            });


        } catch (e) {
            res.status(500);
            return res.send({
                statusCode: 500,
                message: e
            });
        }
    }),

    fastify.get('/forex/rates', async (req, res) => {
        if (req.isCustomerSessionValid(req)) {
            if (req.query.source && req.query.dest) {

                try {
                    if (parseFloat(req.query.amount)) {
                        var resp = await _forex.getRateSourceDest(req.query.source, req.query.dest, req.query.amount)
                        .catch(e => {
                            console.log(e);
                            throw e;
                        });
                    }
                    else {
                        var resp = await _forex.getRateSourceDest(req.query.source, req.query.dest)
                        .catch(e => {
                            console.log(e);
                            throw e;
                        });
                    }

                    console.log(resp);

                    var resObj = {
                        statusCode: 200,
                        rate: resp.rate,
                        source: req.query.source.toUpperCase(),
                        dest: req.query.dest.toUpperCase()
                    }

                    if (resp.result) {
                        resObj.amount = resp.result;
                    }

                    return res.send(resObj);

                } catch (e) {
                    res.status(500);
                    return res.send({
                        statusCode: 500,
                        message: e
                    });
                }
            }
        }
        else {
            res.status(401);
            return res.send({
                statusCode: 401
            });
        }
    }),

    fastify.get('/recovery.html', async (req, res) => {
        res.status(301);
        return res.redirect("/recovery");
    }),

    fastify.get('/recovery', async (req, res) => {
        return res.sendPage(path.join(__dirname, '../', 'pages', 'recovery.html'));
    }),

    fastify.get('/recovery_username.html', async (req, res) => {
        res.status(301);
        return res.redirect("/recovery/username");
    }),

    fastify.get('/recovery/username', async (req, res) => {
        return res.sendPage(path.join(__dirname, '../', 'pages', 'recovery_username.html'));
    }),

    fastify.get('/recovery/finalize', async(req, res) => {
        if (req.query.recovery_key.length == 64) {
            return res.sendPage(path.join(__dirname, '../', 'pages', 'recovery_finalize.html'));
        }
        else {
            return res.redirect('/recovery');
        }
    }),

    fastify.get('/recovery_finalize.html', async(req, res) => {
        res.status(301);
        return res.redirect("/recovery");
    }),

    fastify.get('/onboarding.html', async(req, res) => {
        res.status(301);
        return res.redirect("/onboarding");
    }),

    fastify.get('/onboarding', async(req, res) => {
        return res.sendPage(path.join(__dirname, '../', 'pages', 'onboarding.html'));
    }),

    fastify.get('/transfer_accept.html', async(req, res) => {
        res.status(301);
        return res.redirect("/login");
    }),

    fastify.get('/transfers/:transfer_id', async(req, res) => {
        if (req.isCustomerSessionValid(req)) {

            if (req.session._authenticated == true) {
                return res.sendPage(path.join(__dirname, '../', 'pages', 'transfer_accept.html'));
            } else {
                return res.redirect('/login');
            }
        }
        else {
            return res.redirect('/login');
        }
    })

    
}

module.exports = routes;