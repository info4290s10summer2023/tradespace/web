#!/bin/bash

BUILD_VERSION=$(grep version package.json | awk -F \" '{print $4}')

docker build -t tradespace/web:$BUILD_VERSION -t tradespace/web:latest .