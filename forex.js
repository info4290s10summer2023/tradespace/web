const path = require('path');

if (process.env.NODE_ENV == 'production') {
    require('dotenv').config({
        path: path.join(__dirname, 'master.env')
    });
} else {
    require('dotenv').config({
        path: path.join(__dirname, 'dev.env')
    });
}

const axios = require('axios');

var getRateSourceDest = async (base_symbol, dest_symbol, amount) => {
    try {
        if (base_symbol.length == 3 && dest_symbol.length == 3) {

            var url = `${process.env.FOREX_ENDPOINT}${process.env.FOREX_API_KEY}/pair/${base_symbol}/${dest_symbol}`;

            if (parseFloat(amount)) {
                url += `/${amount}`;
            }
            
            var response = await axios.get(url)
                .catch(e => {
                    throw e;
                });

            console.log(response);

            if (response.data.result == "success") {

                if (response.data.conversion_result) {
                    return {
                        rate: response.data.conversion_rate,
                        result: response.data.conversion_result
                    }
                }
                else {
                    return {
                        rate: response.data.conversion_rate,
                        result: null
                    }
                }
            }
            else {
                throw response.data["error-type"];
            }
        }
        else {
            throw "Invalid base symbol";
        }
    }
    catch (e) {
        throw e;
    }
}

module.exports.getRateSourceDest = getRateSourceDest;