var username = document.getElementById("username");
var recipEmail = document.getElementById("recipEmail");
var transferDate = document.getElementById("lastTransferDate");
var customerMonth = document.getElementById("customerMonth");
var customerYear = document.getElementById("customerYear");

var notifyModalElem = document.getElementById('notifyModal');
var notifyModalLabel = document.getElementById('notifyModalLabel');
var notifyModalSpinner = document.getElementById('notifyModalSpinner');
var notifyModalError = document.getElementById('notifyModalError');
var notifyModal = bootstrap.Modal.getOrCreateInstance(notifyModalElem);

var custCareModalElem = document.getElementById('custCareModal');
var custCareModal = bootstrap.Modal.getOrCreateInstance(custCareModalElem);

var resetModalElem = document.getElementById('resetModal');
var resetModal = bootstrap.Modal.getOrCreateInstance(resetModalElem);
var tempPassDisplay = document.getElementById('tempPass');
var recoveryLink = document.getElementById('recoveryPassFinalize');

document.addEventListener("DOMContentLoaded", e => {
	document.getElementById("recoveryBtn").addEventListener("click", evt => {
		evt.preventDefault();

		setProcessingModal();

		sendRequest("/auth/recovery", "POST", undefined, {
			username: username.value,
			lastTransferDate: transferDate.value,
			recipEmail: recipEmail.value,
			customerMonth: customerMonth.value,
			customerYear: customerYear.value
		}).then(response => {

			notifyModal.hide();

			if (response.recovery_test == "passed") {
				notifyModal.hide();

				tempPassDisplay.textContent = response.temp_passwd;
				recoveryLink.href = '/auth/recovery/finalize?recovery_key=' + response.recovery_key;

				resetModal.show();
			}
			else {
				notifyModal.hide();

				custCareModal.show();
			}
		}).catch(e => {
			notifyModal.hide();
			custCareModal.show();
		});
		

  	});
});

var sendRequest = async (url, method = "GET", params = undefined, body = undefined) => {
    var options = {
        headers: {
            "Content-Type": "application/json"
        },
        method: method.toUpperCase(),
        cache: "no-cache",
        redirect: "follow"
    }

    if (params) {
        if (url.endsWith('/')) {
            url = url.slice(0, -1);
        }

        url = url + "?" + new URLSearchParams(params);
    }

    if (method != "GET") {
        if (body) {
            options.body = new URLSearchParams(body);
        }
    }

    const response = await fetch(url, options)
    .catch(e => {
        throw Error(e);
    });

    var result = response.json();

    return result;
}

var setErrorNotifyModal = (msg) => {
    notifyModal.show();

    notifyModalLabel.textContent = "Error Occurred: " + msg;
    notifyModalSpinner.classList.add('d-none');
    notifyModalError.classList.remove('d-none');
}

var setProcessingModal = (msg = "Evaluating information. Please wait.") => {
    notifyModal.show();

    notifyModalLabel.textContent = msg;
    notifyModalSpinner.classList.remove('d-none');
    notifyModalError.classList.add('d-none');
}


  