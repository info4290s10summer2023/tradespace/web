var fxFromCurrency = document.getElementById("fxFromCurrency");
var fxToCurrency = document.getElementById("fxToCurrency");
var fxRate = document.getElementById("fxRate");

var displayName = document.getElementById("displayName");
var displayEmail = document.getElementById("displayEmail");

var recipList = document.getElementById("recipList");
var recipEditBtn = document.getElementById("recipEditBtn");
var recipAddBtn = document.getElementById("recipAddBtn");
var originAccounts = document.getElementById("originAccounts");
var transferAmount = document.getElementById("transferAmount");
var transferRate = document.getElementById("transferRate");
var transferEstOrigin = document.getElementById("transferEstOrigin");
var transferSecret = document.getElementById("transferSecret");
var toggleTransferSecret = document.getElementById("toggleTransferSecret");
var transferMemo = document.getElementById("transferMemo");
var transferSendBtn = document.getElementById("transferSendBtn");

var recipEdit_suffix = document.getElementById("recipEdit_suffix");
var recipEdit_first = document.getElementById("recipEdit_first");
var recipEdit_middle = document.getElementById("recipEdit_middle");
var recipEdit_last = document.getElementById("recipEdit_last");
var recipEdit_account = document.getElementById("recipEdit_account");
var recipEdit_currency = document.getElementById("recipEdit_currency");
var recipEdit_email = document.getElementById("recipEdit_email");
var recipEdit_identity = document.getElementById("recipEdit_identity");
var recipEdit_identity_country = document.getElementById("recipEdit_identity_country");
var recipEdit_identity_num = document.getElementById("recipEdit_identity_num");
var recipEdit_identity_date = document.getElementById("recipEdit_identity_date");
var recipEditModalBtnSave = document.getElementById("recipEditModalBtnSave");

var recipAdd_account = document.getElementById('recipAdd_account');
var recipAdd_currency = document.getElementById('recipAdd_currency');
var recipAdd_email = document.getElementById('recipAdd_email');
var recipAddModalBtnSave = document.getElementById('recipAddModalBtnSave');

var notifyModalElem = document.getElementById('notifyModal');
var notifyModalLabel = document.getElementById('notifyModalLabel');
var notifyModalSpinner = document.getElementById('notifyModalSpinner');
var notifyModalError = document.getElementById('notifyModalError');
var notifyModalSuccess = document.getElementById('notifyModalSuccess');
var notifyModal = bootstrap.Modal.getOrCreateInstance(notifyModalElem);

var recipSuccessEmail = document.getElementById('recipSuccessEmail');
var recipSuccessInfo = document.getElementById('recipSuccessInfo');


window.addEventListener("DOMContentLoaded", e => {

    toggleTransferSecret.addEventListener("click", e => {
        e.preventDefault();

        if (transferSecret.type == "text") {
            transferSecret.type = "password";
        } else {
            transferSecret.type = "text";
        }
    });

    loadSupportedCurrencies();

    fxToCurrency.addEventListener("change", evt => {
        evt.preventDefault();

        if (fxFromCurrency.value != "null" && fxToCurrency.value != "null") {
            updateRateField(fxFromCurrency.value, fxToCurrency.value);
        }
    });

    populateModalCurrencies();

    populateRecips();

    populateMyAccounts();

    recipEdit_account.addEventListener("change", e => {
        if (recipEdit_account.value.length != 0) {
            recipEdit_currency.disabled = false;
        }
    });

    recipEditBtn.addEventListener("click", ee => {
        ee.preventDefault();
        populateRecipEditModal(recipList.value);
    });

    recipEdit_identity_country.addEventListener("change", e => {
        if (recipEdit_identity_country.value != null && recipEdit_identity_country.children.length <= 1) {
            populateIdentityTypesModal();
        }
    });

    recipList.addEventListener("change", e => {
        e.preventDefault();

        console.log("Recipient Changed");

        recipEditBtn.dataset.index = recipList.value;

        if (originAccounts.value != null && transferAmount.value > 0 && recipList.value != null) {

            var selectedMyAccount = Array.from(originAccounts.children).find(o => {
                if (o.selected == true) {
                    return true;
                } else {
                    return false;
                }
            });

            console.log(selectedMyAccount);
            
            console.log(selectedRecip);

            var selectedRecip = Array.from(recipList.children).find(o => {
                if (o.selected == true) {
                    return true;
                } else {
                    return false;
                }
            });

            populateTransferFXRate(selectedMyAccount.dataset.currency.toUpperCase(), selectedRecip.dataset.currency_code.toUpperCase());

        }

    });

    originAccounts.addEventListener("change", eeee => {

        console.log("My accounts changed");

        var selectedMyAccount = Array.from(originAccounts.children).find(o => {
            if (o.value == originAccounts.value) {
                return true;
            } else {
                return false;
            }
        });

        var selectedRecip = Array.from(recipList.children).find(o => {
            if (o.value == recipList.value) {
                return true;
            } else {
                return false;
            }
        });

        console.log(selectedMyAccount);
        console.log(selectedRecip);

        populateTransferFXRate(selectedMyAccount.dataset.currency.toUpperCase(), selectedRecip.dataset.currency.toUpperCase());

    });

    transferAmount.addEventListener("change", eeeee => {

        console.log("Transfer Amount changed");

        if (transferAmount.value > 0) {
            var transAmt = parseFloat(transferAmount.value);
            var rate = parseFloat(transferRate.value);
    
            var originAmt = transAmt * rate;

            if (isNaN(originAmt)) {
                transferEstOrigin.value = "ERROR";
            } else {
                transferEstOrigin.value = originAmt.toFixed(2);
            }
        }
    });

    transferSendBtn.addEventListener("click", eeeeee => {
        eeeeee.preventDefault();

        createTransfer();
    });

    recipEditModalBtnSave.addEventListener("click", eeeeeee => {
        eeeeeee.preventDefault();

        setProcessingModal("Editing Recipient");

        updateRecipient().then(() => {
            setSuccessNotifyModal("Recipient Changes Saved!");
        });
    });

    recipAdd_account.addEventListener('change', e => {
        e.preventDefault();

        if (recipAdd_account.value.length != 0) {
            recipAdd_currency.disabled = false;
        }
    });

    recipAddModalBtnSave.addEventListener("click", eeeeeee => {
        eeeeeee.preventDefault();

        setProcessingModal("Adding Recipient. Please wait.");

        if (recipAdd_account.value.length != 0 && 
            recipAdd_currency.value != null && 
            recipAdd_email.value.length != 0) {
                createRecipient(() => {
                    setSuccessNotifyModal("Recipient Added!");
                });
        }
    });

});

var sendRequest = async (url, method = "GET", params = undefined, body = undefined) => {
    var options = {
        headers: {
            "Content-Type": "application/json"
        },
        method: method.toUpperCase(),
        cache: "no-cache",
        redirect: "follow"
    }

    if (params) {
        if (url.endsWith('/')) {
            url = url.slice(0, -1);
        }

        url = url + "?" + new URLSearchParams(params);
    }

    if (method != "GET") {
        if (body) {
            options.headers = {
                "Content-Type": "application/x-www-form-urlencoded"
            }
            options.body = new URLSearchParams(body);
        }
    }

    const response = await fetch(url, options)
    .catch(e => {
        throw Error(e);
    });

    var result = response.json();

    return result;
}

var loadSupportedCurrencies = async () => {
    var response = await fetch('/assets/supported_currencies.json');
    var currencies = await response.json();

    currencies.forEach(c => {
        var option = document.createElement("option");
        var option2 = document.createElement("option");

        option.textContent = c.symbol;
        option.value = c.symbol;
        option2.textContent = c.symbol;
        option2.value = c.symbol;


        fxFromCurrency.appendChild(option2);
        fxToCurrency.appendChild(option);
    });
}

var getFXRate = async (from, to, amount = undefined) => {
    var response = await sendRequest(
        '/forex/rates',
        "GET",
        {
            source: from.toUpperCase(),
            dest: to.toUpperCase()
        }, undefined
    );

    console.log(response);

    if (response.statusCode != 200) {
        throw new Error("Error getting rate.");
    }

    return response;
}

var updateRateField = async (from, to) => {
    try {
        var response = await getFXRate(from, to);

        if (!response.rate) {
            throw response.message;
        }

        fxRate.value = response.rate;
    } catch (e) {
        console.log(e);

        fxRate.value = "ERROR";
    }
}

var populateRecips = async () => {
    var response = await sendRequest('/api/v1/db/_get/recipient/list');

    if (response.statusCode != 200) {
        throw response.message;
    }

    console.log(response);

    response.recips.forEach(r => {
        var o = document.createElement("option");

        var display = `${r.suffix_title} ${r.first_name}`;
        if (r.middle_name != null) {
            display += ` ${r.middle_name}`;
        }
        display += ` ${r.last_name} *${String(r.dest_acct).substring(String(r.dest_acct).length-4, String(r.dest_acct).length)}`;

        o.value = r.r_id;
        o.textContent = display;
        o.dataset.acct = r.dest_acct;
        o.dataset.currency = r.currency_code;
        o.dataset.email = r.email_addr;

        recipList.appendChild(o);
    });
}

var populateRecipEditModal = async (r_id) => {

    var r = await sendRequest("/api/v1/db/_get/recipient", "GET", {
        r_id: r_id
    }, undefined).catch(e => {
        throw e;
    });

    console.log(r);

    if (r.statusCode != 200) {
        throw r.message;
    }

    Object.keys(r).forEach(k => {
        if (r[k] == null) {
            r[k] = "";
        }
    });

    recipEdit_suffix.value = r.recip.suffix_title;
    recipEdit_first.value = r.recip.first_name;
    recipEdit_middle.value = r.recip.middle_name;
    recipEdit_last.value = r.recip.last_name;
    recipEdit_account.value = r.recip.dest_acct;
    recipEdit_currency.value = r.recip.currency_code;
    recipEdit_email.value = r.recip.email_addr;
    recipEdit_identity.value = r.recip.identity_type;
    recipEdit_identity_country.value = r.recip.issued_country_code;
    recipEdit_identity_num.value = r.recip.identity_num;
    recipEdit_identity_date.value = r.recip.identity_issue_expiry_date;

    recipEditModalBtnSave.dataset.index = r_id;
}

var updateRecipient = async () => {

    if (recipEdit_identity_date.value.length == 0) {
        recipEdit_identity_date.value = null;
    }

    var response = await sendRequest("/api/v1/db/_update/recipient",
    "POST",
    undefined,
    {
        dest_acct: recipEdit_account.value,
        email: recipEdit_email.value,
        identity_index: recipEdit_identity.value,
        identity_num: recipEdit_identity_num.value,
        identity_date: recipEdit_identity_date.value,
        index: recipEditModalBtnSave.dataset.index
    });

    if (response.statusCode != 200) {
        throw response.message;
    }
    else {
        return response.index;
    }
}

var populateModalCurrencies = async () => {
    var response = await sendRequest('/_db/list/currencies');

    if (response.statusCode != 200) {
        throw new Error(response.message);
    }

    response.currency.forEach(c => {
        var o = document.createElement("option");
        var o2 = document.createElement("option");
        o.value = c.currency_code;
        o2.value = c.currency_code;
        o.textContent = c.currency_code;
        o2.textContent = c.currency_code;

        recipEdit_currency.appendChild(o);
        recipAdd_currency.appendChild(o2);
    });
}

var populateIdentityTypesModal = async (country_code) => {
    var response = await sendRequest('/_db/list/identities_types', "GET", {
        country_code: country_code.toUpperCase()
    }, undefined).catch(e => {
        throw e;
    });

    if (response.statusCode != 200) {
        throw response.message;
    }

    response.identities.forEach(ident => {
        var o = document.createElement("o");
        o.value = ident._name;
        o.textContent = ident._name;

        recipEdit_identity.appendChild(o);
    });
}

var populateMyAccounts = async () => {
    var response = await sendRequest('/api/v1/db/_get/list_accounts');

    if (response.statusCode != 200) {
        throw new Error(response.message);
    }

    response.accounts.forEach(acct => {
        var o = document.createElement("option");
        o.value = acct.account;
        o.textContent = `${acct.account} ${acct.currency_code.toUpperCase()} $${parseFloat(acct.available).toFixed(2)}`;
        o.dataset.currency = acct.currency_code.toUpperCase();

        originAccounts.appendChild(o);
    });
}

var populateTransferFXRate = async (from, to) => {
    try {

        console.log(from);

        console.log(to);

        var response = await getFXRate(from, to);

        console.log(response);

        if (!response.rate) {
            throw response.message;
        }

        transferRate.value = response.rate;
    } catch (e) {
        console.log(e);
        transferRate.value = "ERROR";
    }
}

var createTransfer = async () => {

    setProcessingModal();
    notifyModal.show();

    var selectedRecip = Array.from(recipList.children).find(o => {
        if (o.value == recipList.value) {
            return true;
        } else {
            return false;
        }
    });
    var r_name = selectedRecip.textContent.split("*")[0].trim();
    
    if (transferSecret.value.length <= 0) {
        transferSecret.value = null;
    }

    if (transferMemo.value.length <= 0) {
        transferMemo.value = null;
    }

    var payload = {
        origin_acct: originAccounts.value,
        dest_acct: selectedRecip.dataset.acct,
        recip_index: recipList.value,
        origin_currency: Array.from(originAccounts.children).find(o => o.selected == true).dataset.currency,
        dest_currency: selectedRecip.dataset.currency.toUpperCase(),
        fx_rate: transferRate.value,
        secret: transferSecret.value,
        amount: transferAmount.value,
        origin_amount: transferEstOrigin.value,
        recipient_name: r_name.toUpperCase(),
        recipient_email: selectedRecip.dataset.email.toLowerCase(),
        memo: transferMemo.value,
        sender_name: displayName.value.trim(),
        sender_email: displayEmail.value
    }

    var response = await sendRequest("/api/v1/db/_create/transfer", 
        "POST", 
        undefined, 
        payload
    );

    console.log(payload);

    console.log(response);

    if (!response.transfer_id) {
        recipSuccessInfo.classList.add("d-none");
        setErrorNotifyModal(response.message);
        notifyModal.show();
        throw new Error(response.message);

    } else {
        recipSuccessInfo.classList.remove("d-none");
        recipSuccessEmail.textContent = selectedRecip.dataset.email.toLowerCase();
        setSuccessNotifyModal();

        return;
    }
}

var createRecipient = async () => {
    try {
        var response = await fetch("/api/v1/db/_create/recipient", {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            method: "POST",
            cache: "no-cache",
            redirect: "follow",
            body: new URLSearchParams({
                dest_acct: recipAdd_account.value,
                email: recipAdd_email.value
            })
        });
        
        if (response.status != 201) {
            throw "Error creating Recipient";
        }
        else {

            setSuccessNotifyModal("Recipient Added!");
            return;
        }
    } catch (e) {
        throw e;
    }
}

var setProcessingModal = (msg = "Processing Transfer. Please wait.") => {
    notifyModal.show();

    notifyModalLabel.textContent = msg;
    notifyModalSpinner.classList.remove('d-none');
    notifyModalError.classList.add('d-none');
    notifyModalSuccess.classList.add('d-none');
    recipSuccessInfo.classList.add("d-none");
}

var setErrorNotifyModal = (msg) => {
    notifyModal.show();

    notifyModalLabel.textContent = "Error Occurred: " + msg;
    notifyModalSpinner.classList.add('d-none');
    notifyModalError.classList.remove('d-none');
    notifyModalSuccess.classList.add('d-none');
    recipSuccessInfo.classList.add("d-none");
}

var setSuccessNotifyModal = async (msg = "Transfer Complete!", type = "transfer") => {
    notifyModal.show();

    notifyModalLabel.textContent = msg;
    notifyModalSpinner.classList.add('d-none');
    notifyModalError.classList.add('d-none');
    notifyModalSuccess.classList.remove('d-none');
    if (type != "transfer") {
        recipSuccessInfo.classList.add("d-none");
    } else {
        recipSuccessInfo.classList.remove("d-none");
    }

    await wait(3000);

    notifyModal.hide();

    await wait(1000);

    document.location.reload();
}

var wait = async (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}