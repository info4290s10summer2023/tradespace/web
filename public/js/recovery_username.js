var recoveryBtn = document.getElementById('recoveryBtn');
var email_addr = document.getElementById('email_addr');
var dob = document.getElementById('dob');
var first_name = document.getElementById('first_name');
var last_name = document.getElementById('last_name');
var tradespace_acct = document.getElementById('tradespace_acct');

var processingModalElem = document.getElementById('processingModal');
var processingModal = bootstrap.Modal.getOrCreateInstance(processingModalElem);
var errorModalElem = document.getElementById('errorModal');
var errorModal = bootstrap.Modal.getOrCreateInstance(errorModalElem);
var successModalElem = document.getElementById('successModal');
var successModal = bootstrap.Modal.getOrCreateInstance(successModalElem);

var username_result = bootstrap.Modal.getOrCreateInstance('username_result');



window.addEventListener('DOMContentLoaded', e => {
    
    recoveryBtn.addEventListener('click', evt => {
        evt.preventDefault();

        processingModal.show();

        if (email_addr.value && 
            dob.value && 
            first_name.value && 
            last_name.value && 
            tradespace_acct.value) {
              
              processingModal.show();

              sendResetAttempt().catch(e => {
                  processingModal.hide();
                  errorModal.show();
              });
        }

    });

});

var sendRequest = async (url, method = "GET", params = undefined, body = undefined) => {
    var options = {
        headers: {
            "Content-Type": "application/json"
        },
        method: method.toUpperCase(),
        cache: "no-cache",
        redirect: "follow"
    }

    if (params) {
        if (url.endsWith('/')) {
            url = url.slice(0, -1);
        }

        url = url + "?" + new URLSearchParams(params);
    }

    if (method != "GET") {
        if (body) {
            options.body = new URLSearchParams(body);
        }
    }

    const response = await fetch(url, options)
    .catch(e => {
        throw Error(e);
    });

    var result = response.json();

    return result;
}

sendResetAttempt = async () => {
    var response = await sendRequest('/auth/recovery_username',
        "POST", 
        undefined,
        {
            email: email_addr.value,
            dob: dob.value,
            first_name: first_name.value,
            last_name: last_name.value,
            account: tradespace_acct.value
        });
    
    if (!response.username) {

        throw new Error(response.message);

    } else {

        processingModal.hide();

        username_result.textContent = response.username;
        successModal.show();
    }
}
