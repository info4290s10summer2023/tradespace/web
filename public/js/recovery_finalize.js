var resetPasswdBtn = document.getElementById('restPasswdBtn');
var tempPassInput = document.getElementById('tempPass');
var newPassInput = document.getElementById('newPass');
var newPassCheckInput = document.getElementById('confirmNewPass');

var errorModalElem = document.getElementById('errorModal');
var errorModal = bootstrap.Modal.getOrCreateInstance(errorModalElem);
var errorModalLabel = document.getElementById('errorModalLabel');

var processingModalElem = document.getElementById('processingModal');
var processingModal = bootstrap.Modal.getOrCreateInstance(processingModalElem);

window.addEventListener('DOMContentLoaded', e => {
    var recovery_key = new URLSearchParams(window.location.search).get('recovery_key');
    
    resetPasswdBtn.addEventListener('click', evt => {
        evt.preventDefault();

        if (newPassInput.value == newPassCheckInput.value && tempPassInput.value.length != 0) {

            processingModal.show();

            sendResetAttempt(recovery_key).catch(e => {
                processingModal.hide();

                errorModalLabel.textContent = e;
                errorModal.show();
            });
        }

    });

});

var sendRequest = async (url, method = "GET", params = undefined, body = undefined) => {
    var options = {
        headers: {
            "Content-Type": "application/json"
        },
        method: method.toUpperCase(),
        cache: "no-cache",
        redirect: "follow"
    }

    if (params) {
        if (url.endsWith('/')) {
            url = url.slice(0, -1);
        }

        url = url + "?" + new URLSearchParams(params);
    }

    if (method != "GET") {
        if (body) {
            options.body = new URLSearchParams(body);
        }
    }

    const response = await fetch(url, options)
    .catch(e => {
        throw Error(e);
    });

    var result = response.json();

    return result;
}

sendResetAttempt = async (recovery_key) => {
    var response = await sendRequest('/auth/recovery/reset',
        "POST", 
        undefined,
        {
            recovery_key: recovery_key,
            temp_passwd: tempPassInput.value,
            new_passwd: newPassInput.value
        });
    
    if (!response.finalize) {
        throw response.message;

    } else {
        //password changed successfully
        window.location.replace('/login');

        return;
    }
}
