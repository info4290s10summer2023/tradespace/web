var customerAccountSelect = document.getElementById("customerAccounts");
var accountBalCur = document.getElementById("accountBalCur");
var accountBalAvail = document.getElementById("accountBalAvail");
var transactionPeriodSelect = document.getElementById("transactionPeriod");
var transactionSearch = document.getElementById("transactionSearch");
var transactionsList = document.getElementById("transactionsList");
var transSearchBtn = document.getElementById("transSearchBtn");
var postedDateSortBtn = document.getElementById("postedDateSortBtn");
var postedDateSortIcon = document.getElementById("postedDateSortIcon");

var months = {};
months["January"] = "1";
months["February"] = "2";
months["March"] = "3";
months["April"] = "4";
months["May"] = "5";
months["June"] = "6";
months["July"] = "7";
months["August"] = "8";
months["September"] = "9";
months["October"] = "10";
months["November"] = "11";
months["December"] = "12";

const curMonth = (new Date()).toLocaleString('default', {
    month: 'long'
});
const curMonthText = months[curMonth];
const curYear = new Date().getFullYear();

document.addEventListener('DOMContentLoaded', e => {

    populateAccountList();

    populatePeriods();

    customerAccountSelect.addEventListener('change', (e) => {
        e.preventDefault();

        if (customerAccountSelect.value == "null") {
            transactionPeriodSelect.disabled = true;
            transactionSearch.disabled = true;
        }
        else {
            transactionPeriodSelect.disabled = false;
            transactionSearch.disabled = false;

            setCurAvailBalanceFields();
        }

    });

    transactionPeriodSelect.addEventListener('change', (e) => {
        e.preventDefault();

        if (transactionPeriodSelect.value != null) {
            transactionsList.replaceChildren();

            var selected = Array.from(transactionPeriodSelect.children).find(o => o.value == transactionPeriodSelect.value);
            //yyyy-mm-01
            var m = String(selected.value.split("-")[1]).padStart(2, "0");
            var y = selected.value.split("-")[0];

            populateTransactions(m, y);
        }
    });

    transSearchBtn.addEventListener('click', (e) => {
        e.preventDefault();

        if (transactionSearch.value.length > 0) {
            transactionsList.replaceChildren();
            var o = document.createElement("option");
            o.value = null;
            o.textContent = "Choose";
            transactionsList.appendChild(o);


            const term = transactionSearch.value;

            populateTransactionsSearch(term);
        }
    });

    postedDateSortBtn.addEventListener('click', (e) => {
        e.preventDefault();

        var sort = postedDateSortBtn.dataset.sort;

        if (sort == "desc") {
            postedDateSortBtn.dataset.sort = "asc";
            postedDateSortIcon.classList.remove("fa-arrow-down-9-1");
            postedDateSortIcon.classList.add("fa-arrow-up-1-9");

            sortTransactions("asc");
            //sort the transactions from 1 - 31;
        }
        else {
            postedDateSortBtn.dataset.sort = "desc";
            postedDateSortIcon.classList.remove("fa-arrow-up-1-9");
            postedDateSortIcon.classList.add("fa-arrow-down-9-1");

            sortTransactions("desc");
            //sort the transactions from 31 - 1;
        }
    });


});

var populatePeriods = () => {
    var yearChange = 0;

    for (var i = 0; i <= 8; i++) {

        var thisMonthInt = parseInt(months[curMonth]) - i;

        if (thisMonthInt == 0) {
            thisMonthInt = 12;
            yearChange++;
        }

        console.log(thisMonthInt);

        var o = document.createElement('option');

        thisMonthInt.toString().padStart(2, "0");

        o.value = `${curYear - yearChange}-${thisMonthInt}-01`;

        var textMonth = Object.keys(months).find(k => {
            if (months[k] == parseInt(thisMonthInt)) {
                return true;
            }
        });

        o.textContent = `${textMonth} ${curYear - yearChange}`;

        console.log(o.value);
        console.log(o.textContent);

        transactionPeriodSelect.appendChild(o);
    }
}

var sendRequest = async (url, method = "GET", params = undefined, body = undefined) => {
    var options = {
        headers: {
            "Content-Type": "application/json"
        },
        method: method.toUpperCase(),
        cache: "no-cache",
        redirect: "follow"
    }

    if (params) {
        if (url.endsWith('/')) {
            url = url.slice(0, -1);
        }

        url = url + "?" + new URLSearchParams(params);
    }

    if (method != "GET") {
        if (body) {
            options.headers = {
                "Content-Type": "application/x-www-form-urlencoded"
            }
            options.body = new URLSearchParams(body);
        }
    }

    const response = await fetch(url, options)
    .catch(e => {
        throw Error(e);
    });

    var result = response.json();

    return result;
}

var populateAccountList = async () => {

    try {
        const data = await sendRequest("/api/v1/db/_get/list_accounts");

        if (!data.statusCode == 200) {
            throw data.message;
        }

        data.accounts.forEach(acct => {
            var option = document.createElement('option');
            option.textContent = acct.display;
            option.value = acct.account;
            option.dataset.balance = acct.balance;
            option.dataset.available = acct.available;

            customerAccountSelect.appendChild(option);
        });

        return;

    } catch (e) {
        console.log(e);

        throw e;

        //show error modal
    }
}

var setCurAvailBalanceFields = () => {
    var listedAccounts = Array.from(customerAccountSelect.options);

    var selectedOpt = listedAccounts.find(acct => acct.selected == true);

    if (selectedOpt) {
        accountBalCur.value = selectedOpt.dataset.balance;
        accountBalAvail.value = selectedOpt.dataset.available;
    }
}

var populateTransactions = async (month, year) => {
    try {
        var response = await sendRequest(
            '/api/v1/db/_get/account_transactions', 
            "GET",
            {
                account: customerAccountSelect.value,
                month: month,
                year: year
            }, undefined
        ).catch(e => {
            console.log(e);
            throw e;
        });

        if (response.statusCode != 200) {
            throw response.message;
        }

        console.log(response.trans);
    
        response.trans.forEach((row, i) => {

            console.log(row);

            var tr = document.createElement("tr");
    
            tr.dataset.index = i;
            tr.dataset.id = row.transaction_id;
    
            var td_posted_date = document.createElement("td");
            var td_desc = document.createElement("td");
            var td_wd = document.createElement("td");
            var td_dp = document.createElement("td");
    
            td_posted_date.textContent = row.posted_date.split("T")[0];
            td_desc.textContent = row._desc;
    
            //td_wd
            if (['DR', 'SC'].includes(row.trans_type)) {
                td_wd.textContent = row.trans_amount;
            }
            else {
                //td_dp
                td_dp.textContent = row.trans_amount;
            }
    
            tr.appendChild(td_posted_date);
            tr.appendChild(td_desc);
            tr.appendChild(td_wd);
            tr.appendChild(td_dp);
    
            transactionsList.appendChild(tr);
    
            return;
        });

    } catch (e) {
        throw e;
    }
}

var populateTransactionsSearch = async (term) => {
    var transList = await sendRequest(
        '/api/v1/db/_get/search_transactions', 
        "GET",
        {
            account: customerAccountSelect.value,
            term: term
        }
    ).catch(e => console.log(e));

    transList.forEach(trans, i => {
        var tr = document.createElement("tr");
        tr.dataset.reference = trans.transfer_id;
        tr.dataset.index = i;

        var td_posted_date = document.createElement("td");
        var td_desc = document.createElement("td");
        var td_wd = document.createElement("td");
        var td_dp = document.createElement("td");
        var td_bal = document.createElement("td");

        td_posted_date.textContent = trans.posted_date;
        td_desc.textContent = trans._desc + "<br>" + "@ " + trans.fx_rate;

        //td_wd
        if (['DR', 'SC'].includes(trans.trans_type)) {
            td_wd.textContent = trans.trans_amount;
        }
        else {
            //td_dp
            td_dp.textContent = trans.trans_amount;
        }

        td_bal.textContent = `${trans.balance}`;

        tr.appendChild(td_posted_date);
        tr.appendChild(td_desc);
        tr.appendChild(td_wd);
        tr.appendChild(td_dp);
        tr.appendChild(td_bal);

        transactionsList.appendChild(tr);

        return;
    });
}

var sortTransactions = (direction = "desc") => {
    var rows = Array.from(transactionsList.children);

    if (rows.length != 0) {
        //31 to 1
        if (direction == "desc") {
            if (rows[0].dataset.index <= 1) {
                //asc order
                //change to desc

                var reversed = rows.reverse();

                //wipe all children
                transactionsList.replaceChildren();

                reversed.forEach(_tr => {
                    var tr = document.createElement("tr");

                    Array.from(_tr.children).forEach(_td => {
                        tr.appendChild(_td);
                    });

                    transactionsList.appendChild(tr);
                });
            }
        }
        else {
            if (rows[0].dataset.index >= 1) {
                //desc order
                //change to asc

                var reversed = rows.reverse();

                //wipe all children
                transactionsList.replaceChildren();

                reversed.forEach(_tr => {
                    var tr = document.createElement("tr");

                    Array.from(_tr.children).forEach(_td => {
                        tr.appendChild(_td);
                    });

                    transactionsList.appendChild(tr);
                });
            }
        }
    }

}