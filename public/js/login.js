var username = document.getElementById("username");
var passwd = document.getElementById("passwd");

var alertModalElem = document.getElementById('alertModal');
var alertModalTitle = document.getElementById('alertModalLabel');
var alertModalSpinner = document.getElementById('modalSpinner');
var alertModalError = document.getElementById('modalAlertError');
var alertModal = bootstrap.Modal.getOrCreateInstance(alertModalElem);

document.getElementById("loginForm").addEventListener("submit", function(event) {
    event.preventDefault(); // Prevent the default form submission behavior

    const formData = new URLSearchParams();

    formData.append('username', username.value);
    formData.append('passwd', passwd.value);

    setloadingModal();
    alertModal.show();

    fetch('/auth/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        cache: "no-cache",
        redirect: "follow",
        body: formData
    }).then(response => {
        return response.json();
    }).then(data => {

        console.log(data);

        if (data.statusCode >= 400) {
            setErrorModal("Error. Wrong credentials supplied.");
            alertModal.show();
            passwd.value = "";
        }
        else {
            window.location.href = "/profile";
        }

    }).catch(error => {
        setErrorModal();
        alertModal.show();
        console.error('Error:', error);

        passwd.value = "";
    });
});  

var setloadingModal = () => {
    alertModalSpinner.classList.remove('d-none');
    alertModalError.classList.add('d-none');
    alertModalTitle.textContent = "Loading. Please wait.";
}

var setErrorModal = async (msg = "Internal error occured. Please try again.") => {
    alertModalSpinner.classList.add('d-none');
    alertModalError.classList.remove('d-none');
    alertModalTitle.textContent = msg;

    await wait(3000);
    alertModal.hide();
}

var wait = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}