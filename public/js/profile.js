var suffixInput = document.getElementById('suffix');
var firstNameInput = document.getElementById('firstName');
var lastNameInput = document.getElementById('lastName');
var middleNameInput = document.getElementById('middleName');
var personalAddressLine1Input = document.getElementById('personalAddressLine1');
var personalAddressLine2Input = document.getElementById('personalAddressLine2');
var personalCityInput = document.getElementById('personalCity');
var personalLocaleInput = document.getElementById('personalLocale');
var personalPostalInput = document.getElementById('personalPostal');
var personalCountryInput = document.getElementById('personalCountry');
var personalDOBMonthInput = document.getElementById('personalDOBMonth');
var personalDOBDayInput = document.getElementById('personalDOBDay');
var personalDOBYearInput = document.getElementById('personalDOBYear');
var personalIdentityCountryInput = document.getElementById('personalIdentityCountry');
var personalIdentityTypeInput = document.getElementById('personalIdentityType');
var personalIdentityNumberInput = document.getElementById('personalIdentityNumber');
var personalIdentityDateInput = document.getElementById('personalIdentityDate');
var savePersonalDetailsBtn = document.getElementById('savePersonalDetailsBtn');
var editPersonalDetailsBtn = document.getElementById('editPersonalDetailsBtn');

var personalPhoneInput = document.getElementById('personal_phone');
var personalPhoneAltInput = document.getElementById('personal_phone_alt');
var personalEmailInput = document.getElementById('personal_email_addr');
var saveContactDetailsBtn = document.getElementById('saveContactBtn');
var editContactDetailsBtn = document.getElementById('editContactBtn');

var companyNameInput = document.getElementById('companyName');
var employAddressFullInput = document.getElementById('employAddressFull');
var employPhoneInput = document.getElementById('employPhone');
var employIndustryInput = document.getElementById('employIndustry');
var positionNameInput = document.getElementById('positionName');
var saveEmploymentDetailsBtn = document.getElementById('saveEmploymentBtn');
var editEmploymentDetailsBtn = document.getElementById('editEmploymentBtn');

var _2fa_settingInput = document.getElementById('2fa_setting');
var personalUsernameInput = document.getElementById('personalUsername');
var saveSecurityDetailsBtn = document.getElementById('saveSecurityBtn');
var editSecurityDetailsBtn = document.getElementById('editSecurityBtn');

var alertModalElem = document.getElementById('alertModal');
var alertModalTitle = document.getElementById('alertModalLabel');
var alertModalSpinner = document.getElementById('modalSpinner');
var alertModalError = document.getElementById('modalAlertError');
var alertModal = bootstrap.Modal.getOrCreateInstance(alertModalElem);

document.addEventListener('DOMContentLoaded', e => {

    populatePersonalDetails();

    populateContactDetails();

    populateEmploymentDetails();

    populateSecurityDetails();

    personalIdentityCountryInput.addEventListener("change", e => {
        if (personalIdentityCountryInput.value != null) {

            personalIdentityTypeInput.replaceChildren();
            var o = document.createElement("option");
            o.value = null;
            o.textContent = "Choose";
            o.disabled = true;
            personalIdentityTypeInput.appendChild(o);

            populateIdentityTypes(personalIdentityCountryInput.value.toUpperCase());
        }
    });

    editPersonalDetailsBtn.addEventListener('click', togglePersonalDetailsInputs);
    editContactDetailsBtn.addEventListener('click', toggleContactDetailsInputs);
    editEmploymentDetailsBtn.addEventListener('click', toggleEmployDetailsInputs);
    editSecurityDetailsBtn.addEventListener('click', toggleSecurityDetailsInputs);

    savePersonalDetailsBtn.addEventListener('click', (e) => {
        disablePersonalDetailsInputs(e);
        saveChangesPersonalDetails();
    });

    saveContactDetailsBtn.addEventListener('click', (e) => {
        disableContactDetailsInputs(e);
        saveChangesContactDetails();
    });

    saveEmploymentDetailsBtn.addEventListener('click', (e) => {
        disableEmployDetailsInputs(e);
        saveChangesEmployDetails();
    });

    saveSecurityDetailsBtn.addEventListener('click', (e) => {
        disableSecurityDetailsInputs(e);
        saveChangesSecurityDetails();
    });

});

var sendRequest = async (url, method = "GET", params = undefined, body = undefined) => {
    var options = {
        headers: {
            "Content-Type": "application/json"
        },
        method: method.toUpperCase(),
        cache: "no-cache",
        redirect: "follow"
    }

    if (params) {
        if (url.endsWith('/')) {
            url = url.slice(0, -1);
        }

        url = url + "?" + new URLSearchParams(params);
    }

    if (method != "GET") {
        if (body) {
            options.body = new URLSearchParams(body);
        }
    }

    const response = await fetch(url, options)
    .catch(e => {
        throw Error(e);
    });

    var result = response.json();

    return result;
}

var populatePersonalDetails = async () => {
    try {
        var response = await sendRequest('/api/v1/db/_get/personal_details')
        .catch(e => {
            console.log(e);
            throw e;
        });

        if (response.statusCode != 200) {
            console.log(response);
            throw response.message;
        }

        console.log(response);

        suffixInput.value = response.customer.suffix_title;
        firstNameInput.value = response.customer.first_name;
        middleNameInput.value = response.customer.middle_name;
        lastNameInput.value = response.customer.last_name;
        personalAddressLine1Input.value = response.address.address_line_1;
        personalAddressLine2Input.value = response.address.address_line_2;
        personalCityInput.value = response.address.city;
        personalLocaleInput.value = response.address._locale;
        personalPostalInput.value = response.address.postal_code;

        Array.from(personalCountryInput.children).forEach(o => {
            if (o.value == response.address.country_code) {
                o.selected = true;
            }
        });

        personalCountryInput.value = response.address.country_code;

        var dob = {}
        response.customer.dob = response.customer.dob.split("T")[0];
        dob.year = response.customer.dob.split("-")[0];
        dob.month = parseInt(response.customer.dob.split("-")[1]);
        dob.day = parseInt(response.customer.dob.split("-")[2]);

        Array.from(personalDOBMonthInput.children).forEach(o => {
            if (o.value == dob.month) {
                o.selected = true;
            }
        });

        personalDOBMonthInput.value = dob.month;
        personalDOBDayInput.value = dob.day;
        personalDOBYearInput.value = dob.year;

        Array.from(personalIdentityCountryInput.children).forEach(o => {
            if (o.value == response.identity.issued_country_code) {
                o.selected = true;
            }
        });
        personalIdentityCountryInput.value = response.identity.issued_country_code;

        await populateIdentityTypes(personalIdentityCountryInput.value);
        Array.from(personalIdentityTypeInput.children).forEach(o => {
            if (o.value == response.identity.identity_type) {
                o.selected = true;
            }
        });
        personalIdentityTypeInput.value = response.identity.identity_type;

        personalIdentityNumberInput.value = response.identity._number;

        if (response.identity.expiry_issue_date == "") {
            personalIdentityDateInput.value = "N/A";
        }
        else {
            personalIdentityDateInput.value = response.identity.expiry_issue_date.split("T")[0];
        }
    } catch (e) {
        throw e;
    }
}

var populateContactDetails = async () => {
    try {
        var response = await sendRequest('/api/v1/db/_get/personal_details')
        .catch(e => {
            console.log(e);
            throw e;
        });

        if (response.statusCode != 200) {
            console.log(response);
            throw response.message;
        }

        if (response.customer.primary_phone == null) {
            personalPhoneInput.value = "";
        }
        else {
            personalPhoneInput.value = response.customer.primary_phone;
        }
        
        if (response.customer.alt_phone == null) {
            personalPhoneAltInput.value = "";
        }
        else {
            personalPhoneAltInput.value = response.customer.alt_phone;
        }

        personalEmailInput.value = response.customer.email_addr;
    } catch (e) {
        throw e;
    }

    return;
}

var populateEmploymentDetails = async () => {
    var response = await sendRequest("/api/v1/db/_get/employment_details");

    if (response.statusCode == 200) {
        companyNameInput.value = response.employment.company_name;

        if (response.employment.emp_addr_full == null) {
            employAddressFullInput.value = "";
        } else {
            employAddressFullInput.value = response.employment.emp_addr_full;
        }

        if (response.employment.emp_phone == null) {
            employPhoneInput.value = "";
        } else {
            employPhoneInput.value = response.employment.emp_phone;
        }

        if (response.employment.industry == null) {
            employIndustryInput.value = "";
        } else {
            employIndustryInput.value = response.employment.industry;
        }

        if (response.employment._position_name == null) {
            positionNameInput.value = "";
        } else {
            positionNameInput.value = response.employment._position_name;
        }
    } else {
        throw response.message;
    }
}

var populateSecurityDetails = async() => {
    var response = await sendRequest("/api/v1/db/_get/security_details");

    if (response.statusCode != 200) {
        console.log(response);
        throw response.message;
    }

    Array.from(_2fa_settingInput.children).forEach(o => {
        if (o.value = response.security._2fa_type) {
            o.selected = true;
        }
    });

    _2fa_settingInput.value = response.security._2fa_type;

    personalUsernameInput.value = response.security.username;
}

var populateIdentityTypes = async (country_code) => {
    var response = await sendRequest("/_db/list/identity_types", 
    "GET", { country_code: country_code.toUpperCase() }, undefined);

    if (response.statusCode == 200) {
        response.identities.forEach(row => {
            var o = document.createElement("option");
            o.value = row._value;
            if (row._territory == null || row._territory == "null" || row._territory == "") {
                o.textContent = row._name;
            } else {
                o.textContent = `${row._name} - (${row._territory})`;
            }

            personalIdentityTypeInput.appendChild(o);
        });
    } else {
        throw response.message;
    }
}

var disableSecurityDetailsInputs = (e) => {
    e.preventDefault();

    _2fa_settingInput.disabled = true;
    personalUsernameInput.disabled = true;
}

var disableEmployDetailsInputs = (e) => {
    e.preventDefault();

    companyNameInput.disabled = true;
    employAddressFullInput.disabled = true;
    employPhoneInput.disabled = true;
    positionNameInput.disabled = true;
}

var disableContactDetailsInputs = (e) => {
    e.preventDefault();

    personalPhoneInput.disabled = true;
    personalPhoneAltInput.disabled = true;
    personalEmailInput.disabled = true;
}

var disablePersonalDetailsInputs = (e) => {
    e.preventDefault();

    suffixInput.disabled = true;
    firstNameInput.disabled = true;
    lastNameInput.disabled = true;
    middleNameInput.disabled = true;
    personalAddressLine1Input.disabled = true;
    personalAddressLine2Input.disabled = true;
    personalCityInput.disabled = true;
    personalLocaleInput.disabled = true;
    personalPostalInput.disabled = true;
    personalCountryInput.disabled = true;
    personalDOBMonthInput.disabled = true;
    personalDOBDayInput.disabled = true;
    personalDOBYearInput.disabled = true;
    personalIdentityCountryInput.disabled = true;
    personalIdentityTypeInput.disabled = true;
    personalIdentityNumberInput.disabled = true;
    personalIdentityDateInput.disabled = true;
}

var toggleSecurityDetailsInputs = (e) => {
    e.preventDefault();

    _2fa_settingInput.disabled = !_2fa_settingInput.disabled;
    personalUsernameInput.disabled = !personalUsernameInput.disabled;
}

var toggleEmployDetailsInputs = (e) => {
    e.preventDefault();

    companyNameInput.disabled = !companyNameInput.disabled;
    employAddressFullInput.disabled = !employAddressFullInput.disabled;
    employPhoneInput.disabled = !employPhoneInput.disabled;
    positionNameInput.disabled = !positionNameInput.disabled;
}

var togglePersonalDetailsInputs = (e) => {
    e.preventDefault();

    suffixInput.disabled = !suffixInput.disabled;
    firstNameInput.disabled = !firstNameInput.disabled;
    lastNameInput.disabled = !lastNameInput.disabled;
    middleNameInput.disabled = !middleNameInput.disabled;
    personalAddressLine1Input.disabled = !personalAddressLine1Input.disabled;
    personalAddressLine2Input.disabled = !personalAddressLine2Input.disabled;
    personalCityInput.disabled = !personalCityInput.disabled;
    personalLocaleInput.disabled = !personalLocaleInput.disabled;
    personalPostalInput.disabled = !personalPostalInput.disabled;
    personalCountryInput.disabled = !personalCountryInput.disabled;
    personalDOBMonthInput.disabled = !personalDOBMonthInput.disabled;
    personalDOBDayInput.disabled = !personalDOBDayInput.disabled;
    personalDOBYearInput.disabled = !personalDOBYearInput.disabled;
    personalIdentityTypeInput.disabled = !personalIdentityTypeInput.disabled;
    personalIdentityCountryInput.disabled = !personalIdentityCountryInput.disabled;
    personalIdentityNumberInput.disabled = !personalIdentityNumberInput.disabled;
    personalIdentityDateInput.disabled = !personalIdentityDateInput.disabled;
}

var toggleContactDetailsInputs = (e) => {
    e.preventDefault();

    personalPhoneInput.disabled = !personalPhoneInput.disabled;
    personalPhoneAltInput.disabled = !personalPhoneAltInput.disabled;
    personalEmailInput.disabled = !personalEmailInput.disabled;
}

var saveChangesSecurityDetails = async () => {
    try {
        setloadingModal();
        alertModal.show();

        const response = await fetch('/api/v1/db/_update/security_details', 
            "POST", undefined,
            {
                _2fa_type: _2fa_settingInput.value,
                username: personalUsernameInput.value
            }).catch(e => {
                setErrorModal();
                alertModal.show();
                throw e;
            });

        if (response.statusCode == 200) {
            alertModal.hide();
            window.location.reload();
        } else {
            throw response.message;
        }

    } catch (e) {
        setErrorModal();
        alertModal.show();
        throw e;
    }
}

var saveChangesPersonalDetails = async () => {
    try {
        setloadingModal();
        alertModal.show();

        if (middleNameInput.value != "") {
            middleNameInput.value = null;
        }
        else {
            middleNameInput.value = middleNameInput.value.toUpperCase();
        }

        if (personalAddressLine2Input.value != "") {
            personalAddressLine2Input.value = null;
        }

        personalCountryInput.value = Array.from(personalCountryInput.children).find(o => {
            if (o.selected == true && o.value != null) {
                return true;
            }
        }).textContent;

        if (!personalIdentityDateInput.value) {
            personalIdentityDateInput.value = null;
        }

        var response = await sendRequest("/api/v1/db/_update/personal_details", 
            "POST", 
            undefined, 
            {
                suffix_title: suffixInput.value.toUpperCase(),
                first_name: firstNameInput.value.toUpperCase(),
                middle_name: middleNameInput.value,
                last_name: lastNameInput.value.toUpperCase(),
                DOBMonth: personalDOBMonthInput.value,
                DOBDay: personalDOBDayInput.value,
                DOBYear: personalDOBYearInput.value,
                address_line_1: personalAddressLine1Input.value,
                address_line_2: personalAddressLine2Input.value,
                city: personalCityInput.value,
                _locale: personalLocaleInput.value,
                postal_code: personalPostalInput.value,
                country: personalCountryInput.value,
                identity_type: personalIdentityTypeInput.value,
                identity_number: personalIdentityNumberInput.value,
                expiry_issue_date: personalIdentityDateInput.value
            }).catch(e => {
                setErrorModal();
                alertModal.show();
            });

        if (response.statusCode == 209) {
            alertModal.hide();
            window.location.reload();
        } else {
            throw response.mesage;
        }

    } catch (e) {
        setErrorModal();
        alertModal.show();
        console.log(e);
    }
}

var saveChangesContactDetails = async () => {
    try {
        setloadingModal();
        alertModal.show();

        if (personalPhoneInput.value == "") {
            personalPhoneInput.value = null;
        }

        if (personalPhoneAltInput.value == "") {
            personalPhoneAltInput.value = null;
        }

        const response = await sendRequest('/api/v1/db/_update/contact_details', 
            "POST", undefined, {
                primary_phone: personalPhoneInput.value,
                alt_phone: personalPhoneAltInput.value,
                email_addr: personalEmailInput.value
            }).catch(e => {
                setErrorModal();
                alertModal.show();
                throw e;
            });

        if (response.statusCode == 200) {
            alertModal.hide();
            window.location.reload();
        } else {
            throw response.message;
        }

    } catch (e) {
        setErrorModal();
        alertModal.show();
        console.log(e);
        throw e;
    }
}

var saveChangesEmployDetails = async () => {
    try {
        setloadingModal();
        alertModal.show();

        if (employAddressFullInput.value = "") {
            employAddressFullInput.value = null;
        }

        if (employPhoneInput.value = "") {
            employPhoneInput.value = null;
        }

        if (employIndustryInput.value = "") {
            employIndustryInput.value = null;
        }

        if (positionNameInput.value = "") {
            positionNameInput.value = null;
        }

        const response = await sendRequest('/api/v1/db/_update/employment_details', 
            "POST", undefined, 
            {
                company_name: companyNameInput.value,
                emp_addr_full: employAddressFullInput.value,
                emp_phone: employPhoneInput.value,
                industry: employIndustryInput.value,
                _position_name: positionNameInput.value
            }).catch(e => {
                setErrorModal();
                alertModal.show();

                throw e;
            });

        if (response.statusCode == 200) {
            alertModal.hide();
            window.location.reload();
        } else {
            throw response.message;
        }

    } catch (e) {
        setErrorModal();
        alertModal.show();
        console.log(e);
        throw e;
    }
}

var setloadingModal = () => {
    alertModalSpinner.classList.remove('d-none');
    alertModalError.classList.add('d-none');
    alertModalTitle.textContent = "Loading. Please wait.";
}

var setErrorModal = async () => {
    alertModalSpinner.classList.add('d-none');
    alertModalError.classList.remove('d-none');
    alertModalTitle.textContent = "Error occured. Changes not saved.";

    await wait(5000);
    alertModal.hide();
}

var wait = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}