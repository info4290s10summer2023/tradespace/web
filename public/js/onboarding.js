var revistingCustomerCheck = document.getElementById('revisting-customer-check');
var fyiCheck = document.getElementById('fyi-check');
var kycCheck = document.getElementById('kyc-check');
var kyc2Check = document.getElementById('kyc2-check');
var acctCheck = document.getElementById('acct-check');
var authCheck = document.getElementById('auth-check');

var onboardSignInBtn = document.getElementById('onboardSignInBtn');
var beginNewCustomerBtn = document.getElementById('beginNewCustomerBtn');
var toKYC1Btn = document.getElementById('toKYC1Btn');

var suffixSelect = document.getElementById('suffixSelect');
var firstNameInput = document.getElementById('firstNameInput');
var middleNameInput = document.getElementById('middleNameInput');
var lastNameInput = document.getElementById('lastNameInput');
var dobInput = document.getElementById('dobInput');
var personalAddrLine1Input = document.getElementById('personalAddrLine1Input');
var personalAddrLine2Input = document.getElementById('personalAddrLine2Input');
var personalCityInput = document.getElementById('personalCityInput');
var personalLocaleInput = document.getElementById('personalLocaleInput');
var personalPostalInput = document.getElementById('personalPostalInput');
var countrySelect = document.getElementById('countrySelect');
var personalPrimaryPhoneInput = document.getElementById('personalPrimaryPhoneInput');
var personalEmailInput = document.getElementById('personalEmailInput');
var toKYC2Btn = document.getElementById('toKYC2Btn');

var empCompanyInput = document.getElementById('empCompanyInput');
var empPositionInput = document.getElementById('empPositionInput');
var empIndustryInput = document.getElementById('empIndustryInput');
var empAddrInput = document.getElementById('empAddrInput');
var empPhoneInput = document.getElementById('empPhoneInput');
var identityCountryCodeInput = document.getElementById('identityCountryCodeInput');
var identityNameInput = document.getElementById('identityNameInput');
var identityNumberInput = document.getElementById('identityNumberInput');
var identityDateInput = document.getElementById('identityDateInput');
var toAcctBtn = document.getElementById('toAcctBtn');

var acctCurrencyInput = document.getElementById('acctCurrencyInput');
var toAuthBtn = document.getElementById('toAuthBtn');

var usernameInput = document.getElementById('usernameInput');
var passwdInput = document.getElementById('passwdInput');
var passwdCheckInput = document.getElementById('passwdCheckInput');

var userFinishBtn = document.getElementById('userFinishBtn');

var createdModalElem = document.getElementById('createdModal');
var createdModal = bootstrap.Modal.getOrCreateInstance(createdModalElem);

var processingModalElem = document.getElementById('processingModal');
var processingModal = bootstrap.Modal.getOrCreateInstance(processingModalElem);

var errorModalElem = document.getElementById('errorModal');
var errorModal = bootstrap.Modal.getOrCreateInstance(errorModalElem);

var wait = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}

var _date = new Date();
_date.setFullYear(_date.getFullYear() - 18);



document.addEventListener('DOMContentLoaded', () => {

    populateAcctCurrencies();

    identityCountryCodeInput.addEventListener('change', e => {
        if (identityCountryCodeInput.value != null) {

            identityNameInput.replaceChildren();
            var o = document.createElement("option");
            o.value = null;
            o.textContent = "Choose";
            identityNameInput.appendChild(o);

            populateIdentitiesByCountry(identityCountryCodeInput.value.toUpperCase());
        }
    });

    dobInput.max = _date.toLocaleDateString("en-ca");

    beginNewCustomerBtn.addEventListener("click", e => {
        e.preventDefault();

        fyiCheck.classList.remove("d-none");
        revistingCustomerCheck.classList.add("d-none");
    });

    toKYC1Btn.addEventListener("click", e => {
        e.preventDefault();
        kycCheck.classList.remove("d-none");
        fyiCheck.classList.add("d-none");
    });

    toKYC2Btn.addEventListener("click", e => {
        e.preventDefault();

        if (validateKYC1Check().valid) {
            kyc2Check.classList.remove("d-none");
            kycCheck.classList.add("d-none");
        }
    });

    toAcctBtn.addEventListener("click", e => {
        e.preventDefault();

        if (validateKYC2Check().valid) {
            acctCheck.classList.remove("d-none");
            kyc2Check.classList.add("d-none");
        }
    });

    toAuthBtn.addEventListener("click", e => {
        e.preventDefault();

        if (!acctCurrencyInput.value || acctCurrencyInput.value == "null") {
            acctCurrencyInput.classList.add('border-danger');

            wait(5000).then(() => {
                acctCurrencyInput.classList.remove('border-danger');
            });
        }
        else {
            authCheck.classList.remove('d-none')
            acctCheck.classList.add("d-none");
        }
    });
    
    userFinishBtn.addEventListener("click", e => {
        e.preventDefault();

        if (!usernameInput.value && usernameInput.value.length == 0) {
            usernameInput.classList.add("border-danger");

            wait(5000).then(() => {
                usernameInput.classList.remove('border-danger');
            });
        }
        
        if (passwdInput.value == passwdCheckInput.value) {
            if (validatePasswd(passwdInput.value)) {

                processingModal.show();

                sendOnboarding().then(() => {
                    processingModal.hide();
                    createdModal.show();
                }).catch(() => {
                    processingModal.hide();
                    errorModal.show();
                });
            }
            else {
                passwdInput.classList.add("border-danger");
                passwdCheckInput.classList.add("border-danger");

                wait(5000).then(() => {
                    passwdInput.classList.remove('border-danger');
                    passwdCheckInput.classList.remove('border-danger');
                });
            }
        }
        else {
            passwdInput.classList.add("border-warning");
            passwdCheckInput.classList.add("border-warning");

            wait(5000).then(() => {
                passwdInput.classList.remove('border-warning');
                passwdCheckInput.classList.remove('border-warning');
            });
        }
    });
    
});


var validateKYC1Check = () => {
    try {
        if (!suffixSelect.value || suffixSelect.value == "null" || suffixSelect.value.length >= 4) {
            suffixSelect.classList.add('border-danger');
            throw {
                suffixSelect: false
            }
        }

        if (!firstNameInput.value || firstNameInput.value.length == 0) {
            firstNameInput.classList.add('border-danger');
            throw {
                first_name: false
            }
        }

        if (!lastNameInput.value || lastNameInput.value.length == 0) {
            lastNameInput.classList.add('border-danger');
            throw {
                last_name: false
            }
        }

        if (!dobInput.value) {
            dobInput.classList.add('border-danger');
            throw {
                dob: false
            }
        }

        if (!personalAddrLine1Input.value || personalAddrLine1Input.value.length == 0) {
            personalAddrLine1Input.classList.add('border-danger');
            throw {
                personalAddrLine1: false
            }
        }

        if (!personalCityInput.value || personalCityInput.value.length == 0) {
            personalCityInput.classList.add('border-danger');
            throw {
                personalCity: false
            }
        }

        if (!personalLocaleInput.value || personalLocaleInput.value.length == 0) {
            personalLocaleInput.classList.add('border-danger');
            throw {
                personalLocale: false
            }
        }

        if (!personalPostalInput.value || personalPostalInput.value.length == 0) {
            personalPostalInput.classList.add('border-danger');
            throw {
                personalPostal: false
            }
        }

        if (!countrySelect.value || countrySelect.value == "null") {
            countrySelect.classList.add('border-danger');
            throw {
                personalCountry: false
            }
        }

        if (!personalPrimaryPhoneInput.value || personalPrimaryPhoneInput.value.length == 0 || isNaN(personalPrimaryPhoneInput.value)) {
            personalPrimaryPhoneInput.classList.add('border-danger');
            throw {
                personalPrimaryPhone: false
            }
        }

        if (!personalEmailInput.value || personalEmailInput.value.length == 0 || !personalEmailInput.value.includes("@")) {
            personalEmailInput.classList.add('border-danger');
            throw {
                personalEmail: false
            }
        }

        return {
            valid: true
        }

    } catch (e) {
        return {
            valid: false
        }
    }
}

var validateKYC2Check = () => {
    try {

        if (!empCompanyInput.value || empCompanyInput.value.length == 0) {
            empCompanyInput.classList.add('border-danger');
            throw {
                empCompanyInput: false
            }
        }

        if (!identityCountryCodeInput.value || identityCountryCodeInput.value == "null") {
            identityCountryCodeInput.classList.add('border-danger');
            throw {
                identityCountryCodeInput: false
            }
        }

        if (!identityNameInput.value) {
            identityNameInput.classList.add('border-danger');
            throw {
                identityNameInput: false
            }
        }

        if (!identityNumberInput.value) {
            identityNumberInput.classList.add('border-danger');
            throw {
                identityNumberInput: false
            }
        }

        if (!identityDateInput.value || identityDateInput.value == "null") {
            identityDateInput.classList.add('border-danger');
            throw {
                identityDateInput: false
            }
        }

        return {
            valid: true
        }


    } catch (e) {
        return {
            valid: false,
            e
        }
    }
}

var validatePasswd = (passwd) => {
    if (passwd.length >= 8 && passwd.length <= 32) {
        if (/\d/.test(passwd)) { //contains numbers
            if (/[A-Z]/.test(passwd)) { //contains uppercase letters
                if (/[a-z]/.test(passwd)) { //contains lowercase letters
                    if (/[!@#$%^&*()-_+=]/.test(passwd)) { //contains special characeters
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}

var sendRequest = async (url, method = "GET", params = undefined, body = undefined) => {
    var options = {
        headers: {
            "Content-Type": "application/json"
        },
        method: method.toUpperCase(),
        cache: "no-cache",
        redirect: "follow"
    }

    if (params) {
        if (url.endsWith('/')) {
            url = url.slice(0, -1);
        }

        url = url + "?" + new URLSearchParams(params);
    }

    if (method != "GET") {
        if (body) {
            options.headers = {
                "Content-Type": "application/x-www-form-urlencoded"
            }
            options.body = new URLSearchParams(body);
        }
    }

    const response = await fetch(url, options)
    .catch(e => {
        throw Error(e);
    });

    var result = response.json();

    return result;
}

var sendOnboarding = async () => {
    var info = {
        customer_suffix: suffixSelect.value,
        customer_first: firstNameInput.value,
        customer_middle: middleNameInput.value,
        customer_last: lastNameInput.value,
        customer_dob: dobInput.value,
        customer_primary_phone: personalPrimaryPhoneInput.value,
        customer_email_addr: personalEmailInput.value,
        personal_address_line1: personalAddrLine1Input.value,
        personal_address_line2: personalAddrLine2Input.value,
        personal_address_city: personalCityInput.value,
        personal_address_locale: personalLocaleInput.value,
        personal_address_postal: personalPostalInput.value,
        personal_address_country: countrySelect.value,
        emp_company: empCompanyInput.value,
        emp_position: empPositionInput.value,
        emp_industry: empIndustryInput.value,
        emp_address: empAddrInput.value,
        emp_phone: empPhoneInput.value,
        identity_type: identityNameInput.value,
        identity_number: identityNumberInput.value,
        identity_date: identityDateInput.value,
        account_currency: acctCurrencyInput.value,
        username: usernameInput.value,
        passwd: passwdInput.value
    }

    Object.keys(info).forEach(k => {
        if (info[k] == "") {
            info[k] = null;
        }
    });

    console.log(info);


    var response = await sendRequest('/api/v1/db/_create/onboarding/customer', 
    "POST",
    undefined,
    info
    );

    if (response.statusCode >= 400) {
        throw response.message;
    }

    if (response.statusCode == 201) {
        return;
    }

}

var populateAcctCurrencies = async () => {
    var response = await sendRequest("/_db/list/currencies");

    if (!response.statusCode == 200) {
        throw new Error(response.message);
    }

    response.currency.forEach(c => {
        var o = document.createElement("option");
        o.value = c.currency_code;
        o.textContent = c.full_name;

        acctCurrencyInput.appendChild(o);
    });

}

var populateIdentitiesByCountry = async (country_code) => {
    var response = await sendRequest('/_db/list/identity_types', 
        "GET", 
        {
            country_code: country_code
        });

    if (!response.statusCode == 200) {
        throw new Error(response.message);
    }

    response.identities.forEach(row => {
        var o = document.createElement("option");
        o.value = row._value;
        o.textContent = row._name;

        identityNameInput.appendChild(o);
    });
}