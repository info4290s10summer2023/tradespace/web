var senderName = document.getElementById("senderName");
var transferAmount = document.getElementById("transferAmount");
var transferCurrency = document.getElementById("transferCurrency");
var transferAccount = document.getElementById("transferAccount");
var memo = document.getElementById("memo");

var secretInput = document.getElementById("secretInput");
var secretToggle = document.getElementById("secretToggle");

var depositBtn = document.getElementById("depositBtn");

var processModalElem = document.getElementById("processingModal");
var processModal = bootstrap.Modal.getOrCreateInstance(processModalElem);

var successModalElem = document.getElementById("successModal");
var successModal = bootstrap.Modal.getOrCreateInstance(successModalElem);

var errorModalElem = document.getElementById("errorModal");
var errorModal = bootstrap.Modal.getOrCreateInstance(errorModalElem);
var errorModalLabel = document.getElementById("errorModalLabel");

window.addEventListener("DOMContentLoaded", e => {

    populateInfo(parseInt(window.location.pathname.split("/").pop().split("?")[0]))
    .catch(e => {
        console.log(e);
    });

    secretToggle.addEventListener("click", e => {
        if (secretInput.type == "password") {
            secretInput.type = "text";
        } else {
            secretInput.type = "password";
        }
    });

    depositBtn.addEventListener("click", e => {
        e.preventDefault();

        processModal.show();

        completeTransfer(parseInt(window.location.pathname.split("/").pop().split("?")[0]))
        .then(() => {
            processModal.hide();
            successModal.show();
        }).catch(e => {
            console.log(e);
            processModal.hide();

            if (e.statusCode == 404) {
                errorModal.show();
                errorModalLabel.textContent = e.message;
            } else {
                errorModal.show();
                errorModalLabel.textContent = "Internal Error Occurred";
            }

        });

    });

});

var sendRequest = async (url, method = "GET", params = undefined, body = undefined) => {
    var options = {
        headers: {
            "Content-Type": "application/json"
        },
        method: method.toUpperCase(),
        cache: "no-cache",
        redirect: "follow"
    }

    if (params) {
        if (url.endsWith('/')) {
            url = url.slice(0, -1);
        }

        url = url + "?" + new URLSearchParams(params);
    }

    if (method != "GET") {
        if (body) {
            options.headers = {
                "Content-Type": "application/x-www-form-urlencoded"
            }
            options.body = new URLSearchParams(body);
        }
    }

    const response = await fetch(url, options)
    .catch(e => {
        throw Error(e);
    });

    var result = response.json();

    return result;
}

var populateInfo = async (transfer_id) => {
    try {
        var response = await sendRequest("/api/v1/db/_get/transfer/info", 
        "GET", {
            transfer_id: transfer_id
        }, undefined).catch(e => {
            throw e;
        });
    
        if (!response.statusCode == 200) {

            if (response.statusCode == 401) {
                window.location.replace('/login');
            }

            throw response.message;
        }

        senderName.textContent = response.sender_name;
        transferAmount.textContent = "$" + response.transfer.amount.toFixed(2);
        transferCurrency.textContent = "(" + response.transfer.dest_currency.toUpperCase() + ")";
        transferAccount.textContent = `*${response.transfer.dest_acct.substring(response.transfer.dest_acct.length - 3, response.transfer.dest_acct.length)}`;
        if (response.transfer.memo != null) {
            memo.textContent = response.transfer.memo;
        }

        return;
    } catch (e) {
        throw e;
    }
}


var completeTransfer = async (transfer_id) => {
    try {
        var response = await sendRequest("/api/v1/db/_update/transfer", 
        "POST", undefined, {
            transfer_id: transfer_id,
            secret: secretInput.value
        }).catch(e => {
            throw e;
        });

        if (response != 200) {
            throw response;
        }

        if (response.transfer) {
            return;
        }
    } catch (e) {
        throw e;
    }
}

