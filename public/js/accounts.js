var platformAccountSummary = document.getElementById("platform_account_summary");
var sourceAccountSummary = document.getElementById("source_account_summary");
var sourceAccountEditBtns = document.getElementsByClassName("sourceAccountEditBtn");

var sourceEditModalElem = document.getElementById('sourceEditModal');
var sourceEditModalTitle = document.getElementById('sourceEditModalLabel');
var sourceEditModal = bootstrap.Modal.getOrCreateInstance(sourceEditModalElem);

var sourceBankCountry = document.getElementById('sourceBankCountry');
var sourceBankName = document.getElementById('sourceBankName');
var sourceRouting = document.getElementById('sourceRouting');
var sourceCurrency = document.getElementById('sourceCurrency');
var sourceAccount = document.getElementById('sourceAccount');

var sourceEditRemoveBtn = document.getElementById('sourceEditRemoveBtn');
var sourceEditSaveBtn = document.getElementById('sourceEditSaveBtn');


document.addEventListener('DOMContentLoaded', () => {
    populatePlatformAccounts();

    populateSourceAccounts()
    .then(() => {
        sourceAccountEditBtns = document.getElementsByClassName("sourceAccountEditBtn");
        Array.from(sourceAccountEditBtns).forEach(b => {
            b.addEventListener('click', e => {
                e.preventDefault();
                
                populateSourceEditModal(b.dataset.id);
                sourceEditModal.show();
    
            });
        });
    });

    sourceBankCountry.addEventListener('change', e => {
        e.preventDefault();

        populateSourceBanksBySelectedCountry(sourceBankCountry.value);

    });

    sourceEditSaveBtn.addEventListener('click', e => {
        e.preventDefault();

        saveSourceEdits(sourceEditSaveBtn.dataset.id);

    });

    sourceEditRemoveBtn.addEventListener('click', e => {
        e.preventDefault();

        if (confirm("Are you sure you want to remove this Source Account?")) {
            removeSourceEdit(sourceEditRemoveBtn.dataset.id);
        }

    });

});

var sendRequest = async (url, method = "GET", params = undefined, body = undefined) => {
    var options = {
        headers: {
            "Content-Type": "application/json"
        },
        method: method.toUpperCase(),
        cache: "no-cache",
        redirect: "follow"
    }

    if (params) {
        if (url.endsWith('/')) {
            url = url.slice(0, -1);
        }

        url = url + "?" + new URLSearchParams(params);
    }

    if (method != "GET") {
        if (body) {
            options.body = new URLSearchParams(body);
        }
    }

    const response = await fetch(url, options)
    .catch(e => {
        throw Error(e);
    });

    var result = response.json();

    return result;
}

var populatePlatformAccounts = async () => {
    try {
        var response = await sendRequest('/api/v1/db/_get/list_accounts')
        .catch(e => console.log(e));

        if (!response.statusCode == 200) {
            throw Error(response.message);
        }

        response.accounts.forEach(acct => {
            var tr = document.createElement("tr");

            for (let _td = 1; _td <= 5; _td++) {
                var td = document.createElement("td");

                switch (_td) {
                    case 1:
                        td.textContent = acct.currency_code;
                        break;
                    case 2:
                        td.textContent = acct.account;
                        break;
                    case 3:
                        acct.available = Number(acct.available);
                        var res = String(acct.available).split(".");

                        if (res.length == 1 || res[1].length < 3) {
                            acct.available = acct.available.toFixed(2);
                        }

                        td.textContent = `$ ${acct.available}`;
                        break;
                    case 4:
                        acct.balance = Number(acct.balance);
                        var res = String(acct.balance).split(".");

                        if (res.length == 1 || res[1].length < 3) {
                            acct.balance = acct.balance.toFixed(2);
                        }
                        td.textContent = `$ ${acct.balance}`;
                        break;
                    case 5:
                        var _a = document.createElement("a");
                        _a.classList.add("btn", "btn-sm", "btn-secondary");
                        _a.textContent = "Transactions";
                        _a.href = `/transactions`
                        td.appendChild(_a);
                        break;
                }

                tr.appendChild(td);
            }

            platformAccountSummary.appendChild(tr);
        });
    } catch (e) {
        throw e;
    }

}

var populateSourceAccounts = async () => {
    var response = await sendRequest('/api/v1/db/_get/list_source_accounts')
        .catch(e => {
            throw e;
        });
    
    if (!response.statusCode == 200) {
        throw response.message;
    }

    response.accounts.forEach(acct => {

        var tr = document.createElement("tr");

        for (var _td = 1; _td <= 6; _td++) {
            var td = document.createElement("td");

            switch (_td) {
                case 1:
                    td.textContent = acct.bank;
                    break;
                case 2:
                    td.textContent = acct.country;
                    break;
                case 3:
                    td.textContent = acct.routing_code;
                    break;
                case 4:
                    td.textContent = acct.currency_code;
                    break;
                case 5:
                    td.textContent = acct.account;
                    break;
                case 6:
                    var _a = document.createElement("a");
                    _a.dataset.id = acct.source_id;
                    _a.classList.add("sourceAccountEditBtn");

                    var _i = document.createElement("i");
                    _i.classList.add("fa-regular", "fa-pen-to-square");
                    _a.appendChild(_i);

                    td.appendChild(_a);
                    break;
            }

            tr.appendChild(td);
        }

        sourceAccountSummary.appendChild(tr);
    });

    return;
}

var populateSourceEditModal = async (source_id) => {
    var response = await sendRequest(
        '/api/v1/db/_get/source_account',
        "GET",
        {
            source_id: source_id
        }
    ).catch(e => console.log(e));
    
    if (!response.statusCode == 200) {
        throw response.message;
    }

    sourceEditRemoveBtn.dataset.id = source_id;
    sourceEditSaveBtn.dataset.id = source_id;

    //get currency list populated
    await populateSourceCurrencies();

    //get list of bank countries
    await populateSourceBankCountries();

    var b_countries = Array.from(sourceBankCountry.children);

    //set bank country selected
    var source_country = b_countries.find(o => {
        if (o.value.toUpperCase() == response.source.country.toUpperCase()) {
            o.selected = true;
            return true;
        }
        else {
            false;
        }
    });

    //then whatever is selected from country populate the banks in that country
    await populateSourceBanksBySelectedCountry(source_country.value);
    //whatever selected bank from that country is the bank id (data attr)

    sourceRouting.value = response.source.routing_code;
    
    sourceCurrency.value = response.source.currency_code;

    sourceAccount.value = response.source.account;
}

var populateSourceBankCountries = async () => {
    var response = await sendRequest('/_db/list/bank_countries')
        .catch(e => console.log(e));

    if (!response.statusCode == 200) {
        throw Error(response.message);
    }

    //wipe old options
    sourceBankCountry.replaceChildren();

    response.countries.forEach(c => {
        var o = document.createElement("option");
        o.textContent = c;
        o.value = c;
        sourceBankCountry.appendChild(o);
    });
}

var populateSourceBanksBySelectedCountry = async (country) => {
    var response = await sendRequest(
        '/_db/list/bank',
        'GET',
        {
            country: country
        }
    ).catch(e => console.log(e));

    if (!response.statusCode == 200) {
        throw Error(data.message);
    }

    //wipe old options
    sourceBankName.replaceChildren();

    response.banks.forEach(b => {
        var o = document.createElement('option');
        o.textContent = b.name;
        o.value = b.name;
        o.dataset.id = b.bank_id;
        sourceBankName.appendChild(o);
    });
}

var populateSourceCurrencies = async () => {
    var response = await sendRequest('/_db/list/currencies')
        .catch(e => console.log(e));

    if (!response.statusCode == 200) {
        throw response.message;
    }

    sourceCurrency.replaceChildren();

    response.currency.forEach(c => { 
        var o = document.createElement("option");
        o.textContent = `${c.full_name} - ${c.currency_code}`;
        o.value = c.currency_code;

        sourceCurrency.appendChild(o);
    });
}

var saveSourceEdits = async (source_id) => {

    var response = await sendRequest(
        '/api/v1/db/_update/source_account',
        "POST",
        undefined,
        {
            account: sourceAccount.value,
            currency_code: sourceCurrency.value,
            routing_code: sourceRouting.value,
            bank_id: sourceBankName.value, //bank_id
            source_id: source_id
        }
    ).catch(e => console.log(e));

    if (!response.statusCode == 200) {

        var _modal_dialog = Array.from(sourceEditModalElem.getElementsByClassName("modal-dialog"))[0];
        _modal_dialog.classList.add('border-danger');

        sourceEditModalTitle.classList.add('text-danger');

        await wait(3000);

        _modal_dialog.classList.remove('border-danger');
        sourceEditModalTitle.classList.remove('text-danger');

        throw Error(response.message);


    } else {
        sourceEditModal.hide();

        await wait(1000);
        window.location.reload();
    }
}

var removeSourceEdit = async (source_id) => { 
    var response = await sendRequest(
        '/api/v1/db/_remove/source_account',
        "POST",
        undefined,
        {
            source_id: source_id
        }
    ).catch(e => console.log(e));

    if (!response.statusCode == 200) {

        confirm("Something went wrong. Please try again.");
        throw Error(response.message);

    } else {
        sourceEditModal.hide();

        await wait(1000);
        window.location.reload();
    }
}

var wait = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}
