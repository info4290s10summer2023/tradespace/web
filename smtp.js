const nodemailer = require("nodemailer");
const fs = require('fs-extra');
const path = require('path');

const FROM_ADDR = `"Tradespace Alerts" <${process.env.SMTP_EMAIL}>`;

const transporter = nodemailer.createTransport({
    host: process.env.SMTP_HOST,
    port: process.env.SMTP_PORT,
    secure: true,
    auth: {
        user: process.env.SMTP_EMAIL,
        pass: process.env.SMTP_PASS
    }
});

const OTPresponseHTML = async (code) => {
    try {
        var f = await fs.readFile(path.join(__dirname, 'assets', 'otp.html'), 'utf8');

        //replace placeholder with otp code
        f = f.split('%OTP_CODE%').join(code);

        return f;
    }
    catch (e) {
        throw Error(e);
    }
}

const transferNotifyHTML = async ({
    recip_name,
    sender_name,
    amount,
    currency_code,
    memo,
    transfer_id
}) => {
    try {
        var f = await fs.readFile(path.join(__dirname, 'assets', 'transfer_notify.html'), 'utf8');

        //replace placeholder with otp code
        f = f.split('%RECIP_NAME%').join(recip_name.toUpperCase());
        f = f.split('%SENDER_NAME%').join(sender_name.toUpperCase());
        f = f.split('%AMOUNT%').join(amount);
        f = f.split('%CURRENCY_CODE%').join(currency_code.toUpperCase());
        if (!memo) {
            f = f.split('%MEMO%').join("No message was included.");
        }
        else {
            f = f.split('%MEMO%').join(memo);
        }
        f = f.split('%TRANSFER_ID%').join(transfer_id);

        return f;
    }
    catch (e) {
        throw new Error(e);
    }
}

module.exports = {   
    sendOTP: async ({
        to_addr,
        code
    }) => {
        try {
            const info = await transporter.sendMail({
                from: FROM_ADDR,
                to: [to_addr],
                subject: `Tradespace One-Time Passcode ${code}`,
                html: await OTPresponseHTML(code)
            });
    
            info.catch(e => {
                throw new Error(e);
            });
        }
        catch (e) {
            throw new Error(e);
        }
    },
    sendTransferNotification: async ({
        recip_name,
        to_addr,
        sender_addr,
        sender_name,
        amount,
        dest_currency,
        memo,
        transfer_id
    }) => {
        try {
            await transporter.sendMail({
                from: FROM_ADDR,
                to: [to_addr],
                subject: `A Tradespace transfer from ${sender_name.toUpperCase()} is pending!`,
                replyTo: [sender_addr],
                html: await transferNotifyHTML({
                    recip_name: recip_name,
                    sender_name: sender_name,
                    amount: amount,
                    currency_code: dest_currency,
                    memo: memo,
                    transfer_id: transfer_id
                })
            }).catch(e => {
                throw e;
            });

            return;
        }
        catch (e) {
            throw e;
        }
    }
}