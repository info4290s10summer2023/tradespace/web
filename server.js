const path = require('path');

if (process.env.NODE_ENV == 'production') {
    require('dotenv').config({
        path: path.join(__dirname, 'master.env')
    });
} else {
    require('dotenv').config({
        path: path.join(__dirname, 'dev.env')
    });
}

const fs = require('fs');
const fastify = require('fastify')({ 
    logger: true
});
const fastifySession = require('@fastify/session');
const fastifyCookie = require('@fastify/cookie');
const fastifyStatic = require('@fastify/static');

fastify.register(require('@fastify/formbody'));

fastify.register(require('fastify-mariadb'), {
    host: process.env.MARIADB_HOST,
    user: process.env.MARIADB_USER,
    database: process.env.MARIADB_DATABASE,
    password: process.env.MARIADB_USER_PASSWORD,
    port: process.env.MARIADB_PORT,
    connectionLimit: 5,
    promise: true
});

fastify.register(fastifyCookie, {});
fastify.register(fastifySession, {
    secret: process.env.SESSION_SECRET,
    cookieName: process.env.COOKIE_NAME,
    cookie: {
        secure: false,
        maxAge: 1000*60*20,
        secure: 'auto'
    }
});

fastify.register(require('./routes/pages.js'), {prefix: '/'});
fastify.register(require('./routes/db.js'), {prefix: '/api/v1/db'});
fastify.register(require('./routes/auth.js'), {prefix: '/auth'});

fastify.register(fastifyStatic, {
    root: path.join(__dirname, 'public')
});

//E.g. res.sendPage(path.join('foobar'));
fastify.decorateReply('sendPage', function (fileName) {
    const stream = fs.createReadStream(fileName);
    return this.type('text/html').send(stream);
});

//E.g. req.validAPIKeyHeader();
fastify.decorateRequest('validAPIKeyHeader', function() {
    if (this.headers['x-api-key'] == process.env.DB_API_KEY) {
        return true;
    }
    else {
        return false;
    }
});

fastify.decorateRequest('isCustomerSessionValid', function(request) {
    if (request.session._authenticated && request.session._customer_id.length != 0) {
        return true;
    }
    else {
        return false;
    }
});

fastify.decorateReply('allowAllDomainsCORS', function() {
    return this.headers({
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "*",
        "Access-Control-Allow-Headers": "*"
    });
});

const start = async () => {
    try {
        await fastify.listen({port: process.env.WEB_PORT, host: '0.0.0.0'});
    } catch (e) {
        console.log(e);
        process.exit(1);
    }
}

start();