FROM node:18.12.1-alpine

# Create app directory
WORKDIR /usr/src/app

COPY . .

RUN addgroup node users && \
    addgroup root users && \
    npm install --only=production && \
    chown -R node:users /usr/src/app/

EXPOSE 3000

CMD ["npm", "start"]